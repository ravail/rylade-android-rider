package com.senarios.ryladerider.viewmodel

import android.app.Application
import android.content.SharedPreferences
import androidx.lifecycle.AndroidViewModel
import com.senarios.ryladerider.Rylade
import com.senarios.rylade.costants.SPConstants

/*view model class which sole purpose is to prepare and provide real time data to activity and fragment its connected to.*/
class SharedVM(application: Application) : AndroidViewModel(application) {
    private lateinit var preference:SharedPreferences;


    init {
        getSharedPreference()

    }


    fun getSharedPreference():SharedPreferences{
        preference=getApplication<Rylade>().getSharedPreferences(SPConstants.SP,SPConstants.MODE)
        return preference
    }






}