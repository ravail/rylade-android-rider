package com.senarios.ryladerider.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.senarios.rylade.costants.Constants
import com.senarios.rylade.costants.SPConstants
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.rylade.retrofit.ApiResponse
import com.senarios.ryladerider.R
import com.senarios.ryladerider.model.*
import com.senarios.ryladerider.retrofit.NetworkCall
import com.senarios.ryladerider.utility.Utility
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONObject
import retrofit2.Response
import java.text.DecimalFormat
import kotlin.math.roundToInt

class BackgroundLocation : Service(), ApiResponse {
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private lateinit var location: Location
    private lateinit var builder: LocationSettingsRequest.Builder
    private lateinit var rideModel: RideModel
    private lateinit var orderModel: OrderModel
    private val handler = Handler()
    private lateinit var lastTickLocation: Location
    private lateinit var comparedLocation: Location
    private lateinit var fareCalculationRunnable: Runnable
    private lateinit var timerrunnable: Runnable
    private lateinit var networkDetectionRunnable: Runnable
    private lateinit var startRideRunnable: Runnable
    private lateinit var updateLocationRunnable: Runnable
    private val distance_value: Int = 200
    private val time_value: Long = 30000
    private val results: FloatArray = FloatArray(1)

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        EventBus.getDefault().register(this)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startMyOwnForeground()
        } else {
            startForeground(1, Notification())
        }
        networkDetectionRunnable = Runnable {
            handler.postDelayed(networkDetectionRunnable, 1000)
            if (Utility.isNetworkConnected(this)) {
                if (Utility.getPreferences(this, SPConstants.IS_RIDE, true) as Boolean) {
                    networkAvailable()
                }
            } else {
                networkGone()
                Utility.showLog("Network Gone")
            }
        }

        if (Utility.getPreferences(this, SPConstants.IS_RIDE, true) as Boolean) {
            val list = mutableListOf<ModelCommunication>()
            list.add(ModelCommunication(Constants.USER_ID, Utility.getUser(this).id.toString()))
            NetworkCall.callAPI(this, Utility.getService().checkRideStatus(APIConstants.BEARER + Utility.getUser(this).token, Utility.getJson(list)), this, false, APIConstants.CHECK_RIDE_STATUS)
        }
        handler.postDelayed(networkDetectionRunnable, 1000)

        updateLocationRunnable = Runnable {
            if (isLocation()) {
                val address = Utility.getAddress(this, LatLng(location.latitude, location.longitude))
                if (address != null) {
                    handler.postDelayed(updateLocationRunnable, 45000)
                    Utility.showLog("Location Updated")
                    val list = mutableListOf<ModelCommunication>()
                    list.add(ModelCommunication(Constants.RIDER_ID, Utility.getUser(this).id.toString()))
                    list.add(ModelCommunication(Constants.ADDRESS, address))
                    list.add(ModelCommunication(Constants.LAT, location.latitude.toString()))
                    list.add(ModelCommunication(Constants.LONG, location.longitude.toString()))
                    NetworkCall.callAPI(this, Utility.getService().updateLocation(APIConstants.BEARER + Utility.getUser(this).token, Utility.getJson(list)), this, false, APIConstants.UPDATE_LOCATION)
                }
            }
        }

        handler.postDelayed(updateLocationRunnable, 45000)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        try {
            if (!isLocationCallback()) {
                createLocationRequest()
            }
            if (intent!!.extras != null) {
                rideModel = Utility.getgson().fromJson(intent.getStringExtra(Constants.RIDER_DATA), RideModel::class.java)
                orderModel = Utility.getgson().fromJson(intent.getStringExtra(Constants.ORDER_DATA), OrderModel::class.java)
                Utility.setPreferences(this, SPConstants.ORDER, Utility.getgson().toJson(orderModel))
                Utility.setPreferences(this, SPConstants.RIDE, Utility.getgson().toJson(rideModel))
                Utility.showLog(Utility.getgson().toJson(rideModel))
                Utility.showLog(Utility.getgson().toJson(orderModel))
            }

        } catch (e: Exception) {
            stopService()
        }


        return START_REDELIVER_INTENT
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest.create()
        locationRequest.interval = 500
        locationRequest.fastestInterval = 500
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                if (p0 != null) {
                    location = p0.lastLocation
                    if (isLocation()) {
                        EventBus.getDefault().post(location)
                    }
                }
            }

        }
        requestLocation()
    }

    private fun isLocation(): Boolean {
        return ::location.isInitialized
    }

    private fun isLastTick(): Boolean {
        return ::lastTickLocation.isInitialized
    }

    private fun requestLocation() {
        LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper())

    }

    private fun removeRequestLocation() {
        LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(locationCallback)

    }

    private fun isLocationCallback(): Boolean {
        return ::locationCallback.isInitialized
    }

    private fun getCurrentLatLng(): LatLng {
        return LatLng(location.latitude, location.longitude)
    }



    private fun stopService() {
        if (isLocationCallback()) {
            removeRequestLocation()
        }
        stopForeground(true)
        stopSelf()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (::fareCalculationRunnable.isInitialized) {
            handler.removeCallbacks(fareCalculationRunnable)

        }
        if (::networkDetectionRunnable.isInitialized) {
            handler.removeCallbacks(networkDetectionRunnable)
        }
        if (::timerrunnable.isInitialized) {
            handler.removeCallbacks(timerrunnable)
        }
        if (::updateLocationRunnable.isInitialized){
            handler.removeCallbacks(updateLocationRunnable)
        }
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN) fun rideStatus(model: ModelCommunication) {
        if (model.key.equals(Constants.END_RIDE, true)) {
            onRideEnded()
        } else if (model.key.equals(Constants.ON_SITE, true)) {
            onArrived()
        } else if (model.key.equals(Constants.START_RIDE, true)) {
            Utility.showLog("start Ride Called")
        } else if (model.key.equals(Constants.STOP_TIMER, true)) {
            onStopWaiting()
        }
    }

    private fun onRideEnded(){Utility.showNotification(this, "Ride", "Ride Completed Successfully")
        Utility.clearRideValues(this)
        if (::fareCalculationRunnable.isInitialized) {
            handler.removeCallbacks(fareCalculationRunnable)
        }

    }

    private fun onStopWaiting(){
        Utility.showLog("Waiting timer stopped")
        if (::timerrunnable.isInitialized) {
            handler.removeCallbacks(timerrunnable)
        }
        var timer = 0
        startRideRunnable = Runnable {
            handler.postDelayed(startRideRunnable, 1000)
            timer++
            Utility.showLog("Start RIde Timer Running"+timer)
            if (timer == 60) {
                startRide()
                Utility.showLog("Calculation Ride Called")
                if (::startRideRunnable.isInitialized) {
                    handler.removeCallbacks(startRideRunnable)
                }
            }
        }
        handler.postDelayed(startRideRunnable, 1000)
        Utility.setPreferences(this, SPConstants.IS_RIDE, true)
        val waiting_time = Utility.getPreferences(this, SPConstants.TIMER, 0) as Int
        var initial_fare = waiting_time / 60.0
        initial_fare += 25
        Utility.setPreferences(this, SPConstants.INITIAL_FARE, "" + initial_fare)
        postFare(0, "0")
    }

    private fun onArrived(){
        Utility.showLog("Arrived Called")
        timerrunnable = Runnable {
            handler.postDelayed(timerrunnable, 1000)
            var timer = Utility.getPreferences(this, SPConstants.TIMER, 0) as Int
            timer++
            Utility.showLog("Waiting Timer "+timer)
            Utility.setPreferences(this, SPConstants.TIMER, timer)
        }
        handler.postDelayed(timerrunnable, 1000)
    }

    private fun startRide() {
        lastTickLocation = location
        Utility.setPreferences(this, SPConstants.LAST_LAT_LNG, Utility.getgson().toJson(lastTickLocation))
        Utility.showLog("Calculation Scheduled")
        scheduleCalculation()
    }

    private fun scheduleCalculation() {
            fareCalculationRunnable = Runnable {
                handler.postDelayed(fareCalculationRunnable, time_value)
                if (Utility.isNetworkConnected(this)) {
                    calculateFare()
                }

            }
            handler.postDelayed(fareCalculationRunnable, time_value)
        }



    private fun calculateFare() {
        Utility.showLog("Calculation Called")
        var reminder = Utility.getPreferences(this, SPConstants.REMAINING_M, 0) as Int
        var fare = Utility.getPreferences(this, SPConstants.LAST_FARE, "") as String
        if (fare.isEmpty()) {
            fare = "0.0"
        }
        if (checkDistanceTick()) {
            if (reminder > distance_value) {
                reminder += results[0].roundToInt() - distance_value
                fare = (fare.toDouble() + 2).toString()
            } else if (reminder == distance_value) {
                fare = (fare.toDouble() + 2).toString()
            }
            fare = (fare.toDouble() + 2).toString()
            Utility.setPreferences(this, SPConstants.REMAINING_M, reminder)

        } else {
            fare = (fare.toDouble() + .5).toString()
            Utility.setPreferences(this, SPConstants.REMAINING_M, 0)
        }

        if (comparedLocation.accuracy.roundToInt() < 15) {
            var meter = Utility.getPreferences(this, SPConstants.TOTAL_M, "") as String
            if (meter.isEmpty()) {
                meter = "0.0"
            }
            meter = (results[0] + meter.toDouble()).toString()
            Utility.showLog("" + meter)
            Utility.setPreferences(this, SPConstants.TOTAL_M, meter)
        }

        Utility.setPreferences(this, SPConstants.LAST_FARE, fare)
        var distance = Utility.getPreferences(this, SPConstants.TOTAL_M, "") as String
        if (distance.isEmpty()) {
            distance = "0.0"
        }
        distance = "" + DecimalFormat("##.##").format(distance.toDouble().roundToInt() / 1000.0)
        postFare(fare.toDouble().toInt(), distance)
        Utility.showLog("Distance" + distance)
        Utility.showLog("Fare" + fare)
        lastTickLocation = comparedLocation
        Utility.setPreferences(this, SPConstants.LAST_LAT_LNG, Utility.getgson().toJson(lastTickLocation))
        Utility.setPreferences(this, SPConstants.LAST_TIME, "" + System.currentTimeMillis())
        postValuesToServer()
    }

    private fun checkDistanceTick(): Boolean {
        try {
            if (isLocation() && isLastTick()) {
                comparedLocation = location
                Location.distanceBetween(lastTickLocation.latitude, lastTickLocation.longitude, comparedLocation.latitude, comparedLocation.longitude, results)
                if (results[0].roundToInt() == distance_value) {
                    return true
                } else if (results[0].roundToInt() > distance_value) {
                    var reminder = Utility.getPreferences(this, SPConstants.REMAINING_M, 0) as Int
                    reminder += results[0].roundToInt() - distance_value
                    Utility.setPreferences(this, SPConstants.REMAINING_M, reminder)
                    return true
                }

            }
            return false
        } catch (e: Exception) {
            return false
        }
    }

    private fun networkGone() {
        Utility.setPreferences(this, SPConstants.NETWORK_BOOT_FLAG, true)
    }

    private fun networkAvailable() {
        if (isLocation()) {
            if (Utility.getPreferences(this, SPConstants.NETWORK_BOOT_FLAG, false) as Boolean) {
                if (Utility.getPreference(this).contains(SPConstants.LAST_LAT_LNG) && Utility.getPreference(this).contains(SPConstants.LAST_TIME)) {
                    Utility.showLog("Ride called from oncreate/network resume")
                    lastTickLocation = Utility.getgson().fromJson<Location>(Utility.getPreferences(this, SPConstants.LAST_LAT_LNG, "") as String, Location::class.java)
                    val lasttime = calculateMinutesFromMillis((Utility.getPreferences(this, SPConstants.LAST_TIME, "") as String).toLong())
                    val distance = calculateDistance(LatLng(location.latitude, location.longitude), LatLng(lastTickLocation.latitude, lastTickLocation.longitude))
                    var fare = Utility.getPreferences(this, SPConstants.LAST_FARE, "") as String
                    if (fare.isEmpty()) {
                        fare = "0"
                    }
                    if (lasttime == distance) {
                        fare = (fare.toDouble() + distance * 4).toString()
                    } else if (lasttime > distance) {
                        fare = (fare.toDouble() + lasttime).toString()
                    } else if (lasttime < distance) {
                        fare = (fare.toDouble() + distance * 4).toString()
                    }
                    Utility.setPreferences(this, SPConstants.LAST_FARE, fare)
                    Utility.showLog("Fare" + fare)
                    Utility.removeValueFromSP(this, SPConstants.NETWORK_BOOT_FLAG)
                    rideModel = Utility.getgson().fromJson(Utility.getPreferences(this, SPConstants.RIDE, "") as String, RideModel::class.java)
                    orderModel = Utility.getgson().fromJson(Utility.getPreferences(this, SPConstants.ORDER, "") as String, OrderModel::class.java)
                    Utility.showLog(Utility.getgson().toJson(rideModel))
                    Utility.showLog(Utility.getgson().toJson(orderModel))
                    scheduleCalculation()

                }
            }
        }
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        if (!(Utility.getPreferences(this,SPConstants.IS_RIDE,false) as Boolean)) {
            stopService()
        }
    }

    private fun calculateMinutesFromMillis(millis: Long): Double {
        val seconds = (System.currentTimeMillis() - millis) / 1000
        return seconds / 60.0
    }

    private fun calculateDistance(fLatlng: LatLng, sLatLng: LatLng): Double {
        Location.distanceBetween(fLatlng.latitude, fLatlng.longitude, sLatLng.latitude, sLatLng.longitude, results)
        return results[0] / 1000.0
    }

    private fun isRide(): Boolean {
        return ::rideModel.isInitialized
    }

    private fun postFare(fare: Int, distance: String) {
        var initialFare = Utility.getPreferences(this, SPConstants.INITIAL_FARE, "") as String
        if (initialFare.isEmpty()) {
            initialFare = "0"
        }
        val totalFare = "" + (initialFare.toDouble().roundToInt() + fare)
        val model = Fare(totalFare.toDouble().roundToInt(), distance)
        EventBus.getDefault().post(model)
    }

    private fun postValuesToServer() {
        try {
            val list = mutableListOf<ModelCommunication>()
            list.add(ModelCommunication(Constants.USER_ID, Utility.getUser(this).id.toString()))
            list.add(ModelCommunication(Constants.ORDER_ID, orderModel.id.toString()))
            list.add(ModelCommunication(Constants.INITIAL_FARE, "" + Utility.getPreferences(this, SPConstants.INITIAL_FARE, "").toString().toDouble().roundToInt()))
            list.add(ModelCommunication(Constants.CALCULATED_FARE, "" + Utility.getPreferences(this, SPConstants.LAST_FARE, "").toString().toDouble().roundToInt()))
            list.add(ModelCommunication(Constants.WAITING_TIME, Utility.getPreferences(this, SPConstants.TIMER, 0).toString()))
            list.add(ModelCommunication(Constants.WAITING_COST, "" + (Utility.getPreferences(this, SPConstants.TIMER, 0).toString().toInt() / 60.0).roundToInt()))
            list.add(ModelCommunication(Constants.LAST_TIME, "" + Utility.getPreferences(this, SPConstants.LAST_TIME, "").toString()))
            list.add(ModelCommunication(Constants.LAST_LAT, lastTickLocation.latitude.toString()))
            list.add(ModelCommunication(Constants.LAST_LNG, lastTickLocation.longitude.toString()))
            NetworkCall.callAPI(this, Utility.getService().changeRideStatus(APIConstants.BEARER + Utility.getUser(this).token, Utility.getJson(list)), this, false, APIConstants.CHANGE_RIDE_STATUS)
        } catch (e: java.lang.Exception) {
            Utility.showLog("issue in posting values" + e.message)
        }

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        if (endpoint.equals(APIConstants.CHECK_RIDE_STATUS)) {
            if (Utility.getPreferences(this, SPConstants.IS_RIDE, true) as Boolean) {
                Utility.showLog("check ride called success Ride Available")
                networkGone()
            }
        } else if (endpoint.equals(APIConstants.CHANGE_RIDE_STATUS)) {
            val json = JSONObject(response)
            if (json.has("order") && json.has("ride_order")) {
                Utility.showLog("update ride status success")
                Utility.setPreferences(this, SPConstants.ORDER, json.getJSONObject("order").toString())
                Utility.setPreferences(this, SPConstants.RIDE, json.getJSONObject("ride_order").toString())
                orderModel = Utility.getgson().fromJson(json.getJSONObject("order").toString(), OrderModel::class.java)
                rideModel = Utility.getgson().fromJson(json.getJSONObject("ride_order").toString(), RideModel::class.java)
            }
        }
    }

    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {
        if (endpoint.equals(APIConstants.CHECK_RIDE_STATUS)) {
            Utility.showLog("check ride called failed")
            Utility.clearRideValues(this)
        }
    }

    override fun OnError(endpoint: String, code: Int, message: String) {
        if (endpoint.equals(APIConstants.CHECK_RIDE_STATUS)) {
            retryRideCheck()
        }
    }

    override fun OnException(endpoint: String, exception: Throwable) {
        if (endpoint.equals(APIConstants.CHECK_RIDE_STATUS)) {
            retryRideCheck()
        }
    }

    override fun OnNetworkError(endpoint: String, message: String) {
        if (endpoint.equals(APIConstants.CHECK_RIDE_STATUS)) {
            retryRideCheck()
        }
    }

    private fun retryRideCheck(){
        val list = mutableListOf<ModelCommunication>()
        list.add(ModelCommunication(Constants.USER_ID, Utility.getUser(this).id.toString()))
        NetworkCall.callAPI(this, Utility.getService().checkRideStatus(APIConstants.BEARER + Utility.getUser(this).token, Utility.getJson(list)), this, false, APIConstants.CHECK_RIDE_STATUS)

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun startMyOwnForeground() {
        val NOTIFICATION_CHANNEL_ID = applicationContext.packageName
        val channelName = getString(R.string.rylade_notification)
        val chan = NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_HIGH)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val manager = (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
        manager.createNotificationChannel(chan)
        val notificationBuilder = NotificationCompat.Builder(applicationContext, NOTIFICATION_CHANNEL_ID)
        val notification = notificationBuilder.setOngoing(true).setSmallIcon(R.drawable.rylade).setContentTitle("App is running in background").setPriority(NotificationManager.IMPORTANCE_HIGH)
            .setCategory(Notification.CATEGORY_SERVICE).build()
        startForeground(1, notification)
    }
}