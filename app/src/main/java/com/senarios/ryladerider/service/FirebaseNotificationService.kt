package com.senarios.ryladerider.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.senarios.ryladerider.activities.MainActivity
import io.karn.notify.Notify
import java.lang.Exception

class FirebaseNotificationService : FirebaseMessagingService() {
    private val TAG=javaClass.name

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)

        val set=remoteMessage.data.keys

        if (remoteMessage.data !=null && set.size>0){
            if (set.contains("title") && set.contains("message")){
                showFirebaseNotification(remoteMessage.data)
            }
        }


    }

    private fun showFirebaseNotification(data: Map<String, String>) {
        try {
            val title = data.get("title")
            val message = data.get("message")
            val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                val channel = NotificationChannel("0", "Rylade", NotificationManager.IMPORTANCE_HIGH)
                manager.createNotificationChannel(channel)
            }
            val pending=PendingIntent.getActivity(this,0, Intent(this,MainActivity::class.java),PendingIntent.FLAG_ONE_SHOT)
            val builder = Notify.with(this).asBuilder().setContentTitle(title).setContentText(message).setContentIntent(pending).build()
            manager.notify(0, builder)
        }
        catch (e:Exception){
         Log.v(TAG,""+e.message)
        }


    }


}