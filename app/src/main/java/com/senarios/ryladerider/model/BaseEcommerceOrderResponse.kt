package com.senarios.ryladerider.model

import com.google.gson.annotations.SerializedName

class BaseEcommerceOrderResponse(@SerializedName("items" )var list:MutableList<EcommerceProductModel>,
                                 @SerializedName("address") var address: Address,
    @SerializedName("user") var user:User,
@SerializedName("vendor") var vendor:User)
