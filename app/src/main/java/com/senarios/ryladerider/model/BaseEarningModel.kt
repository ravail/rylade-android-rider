package com.senarios.ryladerider.model


import com.google.gson.annotations.SerializedName

data class BaseEarningModel(
    @SerializedName("data")
    var `data`: List<EarningModel>?,
    @SerializedName("datefrom")
    var datefrom: String?,
    @SerializedName("dateto")
    var dateto: String?
)