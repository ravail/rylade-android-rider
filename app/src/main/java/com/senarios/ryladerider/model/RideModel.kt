package com.senarios.ryladerider.model

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.google.gson.annotations.SerializedName
import com.senarios.rylade.costants.Constants

class RideModel (
    var pickup_address:String?,
    var pickup_lat:String?,
    var pickup_lon:String?,
    var dropoff_address:String?,
    var dropoff_lat:String?,
    var dropoff_lon:String?,
    @SerializedName(Constants.RIDE_DISTANCE) var distance:String?,
    var amount:String?,
    var duration:String?,
    @SerializedName(Constants.ESTIMATE_FARE) val estimate_fare : String?,
    @SerializedName(Constants.INITIAL_FARE) val initial_fare : String?,
    @SerializedName(Constants.TOTAL_FARE) val total_Fare : String?,
    @SerializedName(Constants.ESTIMATE_TIME) val estimate_time : String?,
    @SerializedName(Constants.CALCULATED_FARE) val calculated_fare : String?,
    @SerializedName(Constants.WAITING_TIME) val waiting_time : String?,
    @SerializedName(Constants.WAITING_COST) val waiting_cost : String?,
    @SerializedName(Constants.LAST_TIME) val last_time : String?,
    @SerializedName(Constants.TOTAL_TIME) val total_time : String?,
    @SerializedName(Constants.LAST_LAT) val last_lat : String?,
    @SerializedName(Constants.LAST_LNG) val last_lng : String?,
    @SerializedName(Constants.STATUS) val status : String?
    )


@BindingAdapter(value = arrayOf("setText","settitle"),requireAll = false)
fun setText(view: View, text:String?,title:String?){
    if (text.isNullOrEmpty()){
        view.visibility=View.GONE
    }
    else if (view is TextView){
        view.visibility=View.VISIBLE
        view.text= "$title : $text"
    }
    else{
        view.visibility=View.VISIBLE
    }
}