package com.senarios.ryladerider.model


import android.content.ClipData
import com.google.gson.annotations.SerializedName
import com.senarios.rylade.models.MedicineModel

data class BaseMedicineResponse(
    @SerializedName("address")
    var address: Address?,
    @SerializedName("items")
    var items: MutableList<MedicineModel>?,
    @SerializedName("user")
    var user: User?
)