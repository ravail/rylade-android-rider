package com.senarios.ryladerider.model


import com.google.gson.annotations.SerializedName

data class EarningModel(
    @SerializedName("amount")
    var amount: Int?,
    @SerializedName("month")
    var month: String?
)