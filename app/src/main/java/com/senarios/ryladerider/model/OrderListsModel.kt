package com.senarios.ryladerider.model

import com.google.gson.annotations.SerializedName

class OrderListsModel(@SerializedName("orders") val orders : MutableList<OrderModel>?)
