package com.senarios.ryladerider.model

import android.graphics.Paint
import android.os.Parcelable
import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.room.ColumnInfo
import androidx.room.PrimaryKey
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.gson.annotations.SerializedName
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.ryladerider.utility.Utility
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.parcel.Parcelize
import java.lang.Exception

@Parcelize
class EcommerceProductModel (
    var id:Long?,
    @SerializedName("category_id")
    var categoryId: Int?,
    @SerializedName("category_name")
    var categoryName: String?,
    @SerializedName("created_at")
    var createdAt: String?,
    @SerializedName("description")
    var description: String?,
    @SerializedName("product_id")
    var product_id: Int?,
    @SerializedName("image")
    var image: String?,
    @SerializedName("price")
    var price: Int?,
    @SerializedName("product_name")
    var productName: String?,
    @SerializedName("promo_code_id")
    var promoCodeId: Int?,
    @SerializedName("quantity_available")
    var quantityAvailable: Int?,
    @SerializedName("status")
    var status: Int?,
    @SerializedName("sub_category_name")
    var subCategoryName: String?,
    @SerializedName("updated_at")
    var updatedAt: String?,
    @SerializedName("weight")
    var weight: Int?,
    @SerializedName("vendor_id")
    var vendorId: Int?,
    @SerializedName("vendor_name")
    var vendor_name: String?,
    @SerializedName("quantity")
    var quantity: Int?,
    @SerializedName("order_id")
    var order_id: Int?,
    @SerializedName("sales_tax")
    var sales_tax: Double,
    @SerializedName("fixed_discount")
    var fixed_discount: Int?,
    @SerializedName("iss_discount")
    var iss_discount: Int?,
    var originalPrice: Int?,
    @SerializedName("discounted_price")
    var discounted_price: Int?,
    var isOutStock:Boolean



) : Parcelable
@BindingAdapter("loadProductImage")
fun loadProductImage(view: View, image:String){
    Glide.with(view.context)
        .load(APIConstants.BASE_PRODUCT_PHOTO + image)
        .diskCacheStrategy(DiskCacheStrategy.DATA)
        .apply(RequestOptions().timeout(20000))
        .apply(RequestOptions.overrideOf(100,100))
        .into(view as CircleImageView)
}
@BindingAdapter(value = arrayOf("setprice","pricetype","isDiscount"),requireAll = false)
fun setPrice(view:View?,price: Int?,type:String?,isDiscount : Int?){
    Utility.showLog("Price $price")
    Utility.showLog("type $type")
    Utility.showLog("isDiscount $isDiscount")
    try{
        if (view is TextView) {
            if (type!!.toInt() == 0) {
                view.text = price!!.toString()+" PKR"
                if (isDiscount == 1) {
                    view.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
                }

            } else {
                view.text = price!!.toString()+" PKR"
                if (isDiscount == 1) {
                    view.visibility=View.VISIBLE
                }
            }
        }
    }
    catch (e: Exception){
        e.printStackTrace()
    }
}