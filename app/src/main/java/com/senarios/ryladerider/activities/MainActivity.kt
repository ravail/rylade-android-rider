package com.senarios.ryladerider.activities

import android.annotation.SuppressLint
import android.content.Intent
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.CompoundButton
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.android.material.navigation.NavigationView
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.rylade.costants.Constants
import com.senarios.rylade.costants.Messages
import com.senarios.rylade.costants.SPConstants
import com.senarios.rylade.costants.Tags
import com.senarios.rylade.costants.Tags.Companion.C_ORDER
import com.senarios.rylade.costants.Tags.Companion.EARNING
import com.senarios.rylade.costants.Tags.Companion.P_ORDER
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.rylade.retrofit.ApiResponse
import com.senarios.ryladerider.R
import com.senarios.ryladerider.callbacks.FragmentChanger
import com.senarios.ryladerider.databinding.ActivityMainBinding
import com.senarios.ryladerider.fragments.CurrentOrdersFragment
import com.senarios.ryladerider.fragments.EarningFragment
import com.senarios.ryladerider.fragments.PastOrdersFragment
import com.senarios.ryladerider.fragments.ProfileFragment
import com.senarios.ryladerider.model.ModelBaseResponse
import com.senarios.ryladerider.model.ModelCommunication
import com.senarios.ryladerider.model.User
import com.senarios.ryladerider.retrofit.NetworkCall
import com.senarios.ryladerider.service.BackgroundLocation
import com.senarios.ryladerider.utility.Utility
import retrofit2.Response

class MainActivity : BaseAcivity() ,View.OnClickListener,ApiResponse,CompoundButton.OnCheckedChangeListener,FragmentChanger,NavigationView.OnNavigationItemSelectedListener{
    private lateinit var binding:ActivityMainBinding
    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this,R.layout.activity_main)
    }

    override fun init() {
        binding.navView.setNavigationItemSelectedListener(this)
        binding.toolbar.setNavigationOnClickListener(this)
        binding.navView.inflateMenu(R.menu.main_drawer_menu)

        OnChange(CurrentOrdersFragment(),C_ORDER)

        if (Utility.getUser(this).rider_availability==1){
            binding.container.visibility=View.VISIBLE
            binding.sw.isChecked=true
            if (Utility.hasLocationPermission(this)) {
                startService(Intent(this@MainActivity, BackgroundLocation::class.java))
            }
            else{
                checkLocationPermission()
            }
        }
        else{
            binding.sw.isChecked=false
            binding.container.visibility=View.GONE
        }


        binding.sw.setOnCheckedChangeListener(this)
    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {
        startService(Intent(this@MainActivity, BackgroundLocation::class.java))
    }
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        binding.drawer.closeDrawers()
        when(item.itemId){


            R.id.currentOrders->{
                OnChange(CurrentOrdersFragment(), C_ORDER)
            }
            R.id.pastOrder->{
                OnChange(PastOrdersFragment(), P_ORDER)
            }
            R.id.profile->{
                OnChange(ProfileFragment(), Tags.PROFILE)
            }
            R.id.earning-> {
                OnChange(EarningFragment(), EARNING)
            }
            R.id.tc->{

            }
            R.id.contact_us->{

            }
            R.id.faq->{

            }
            R.id.logout->{
                logout()
            }



        }
        return true
    }

    private fun logout() {
        stopService(Intent(this,BackgroundLocation::class.java))
        val fcm=Utility.getFCM(this)
        Utility.getPreference(this).edit().clear().apply()
        Utility.setPreferences(this,SPConstants.FCM,fcm)
        startActivity(Intent(this,LoginActivity::class.java))
        finish()
    }

    @SuppressLint("WrongConstant")
    override fun onClick(v: View?) {
        binding.drawer.openDrawer(Gravity.START)
    }

    override fun onBackPressed() {
        if (supportFragmentManager.fragments.size > 0) {
            getCallback().OnBackPressed()
        } else {
            super.onBackPressed()
        }
    }

    override fun OnChange(fragment: BaseDialogeFragment) {

    }

    override fun OnTrigger() {

    }

    override fun OnChange(fragment: Fragment, tag: String) {
        binding.toolbar.title=tag
        supportFragmentManager.beginTransaction().replace(binding.container.id,fragment,tag).commitAllowingStateLoss()
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {

        if (isChecked){
            changeRiderStatus(1)
        }
        else{
            changeRiderStatus(0)
        }
    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        val data= Utility.getgson().fromJson<User>(response,User::class.java)

        Utility.setPreferences(this,SPConstants.USER,response)

        if (binding.sw.isChecked){
            binding.container.visibility=View.VISIBLE
            if (Utility.hasLocationPermission(this)) {
                startService(Intent(this@MainActivity, BackgroundLocation::class.java))
            }
            else{
                checkLocationPermission()
            }

        }
        else{
            binding.container.visibility=View.GONE
            stopService(Intent(this,BackgroundLocation::class.java))
        }
    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialog(this@MainActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)

    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        Utility.showErrorDialog(this@MainActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)

    }


    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialog(this@MainActivity,R.layout.dialog_error, Messages.ERROR_TITLE,getString(R.string.something_went_wrong))

    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialogwithFinish(this@MainActivity)
    }
    private fun changeRiderStatus(availability:Int) {
        val list:MutableList<ModelCommunication> = mutableListOf()
        list.add(ModelCommunication(Constants.RIDER_ID, Utility.getUser(this).id.toString()))
        list.add(ModelCommunication(Constants.RIDER_AVAILABILITY, availability.toString()))
        NetworkCall.callAPI(this,
            Utility.getService().changeRiderStatus(APIConstants.BEARER+ Utility.getUser(this).token,Utility.getJson(list)),this, true, APIConstants.CHANGE_RIDER_AVAILABILITY)
    }
}
