package com.senarios.ryladerider.activities

import android.content.Intent
import android.util.Patterns
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.muddzdev.styleabletoast.StyleableToast
import com.senarios.rylade.costants.Constants
import com.senarios.rylade.costants.Messages
import com.senarios.rylade.costants.SPConstants
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.rylade.retrofit.ApiResponse
import com.senarios.ryladerider.R
import com.senarios.ryladerider.databinding.ActivityLoginBinding
import com.senarios.ryladerider.model.*
import com.senarios.ryladerider.retrofit.NetworkCall
import com.senarios.ryladerider.utility.Utility
import org.json.JSONObject
import retrofit2.Response

class LoginActivity : BaseAcivity(), TextView.OnEditorActionListener, View.OnClickListener, View.OnFocusChangeListener,ApiResponse {
    private lateinit var binding:ActivityLoginBinding
    private val list= mutableListOf<ModelCommunication>()


    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this, R.layout.activity_login)
    }

    override fun init() {
        if (Utility.checkIfUserLogged(this)){
            binding.constraintLayout.visibility=View.GONE
            list.clear()
            list.add(ModelCommunication(Constants.USER_ID,Utility.getUser(this).id.toString()))
            NetworkCall.callAPI(this,getService().checkRideStatus(
                APIConstants.BEARER+Utility.getUser(this).token
            ,Utility.getJson(list)
            ),this,true, APIConstants.CHECK_RIDE_STATUS)

        }
        else{
            binding.constraintLayout.visibility=View.VISIBLE
        }

        binding.button.setOnClickListener(this)
        binding.emailEditText.onFocusChangeListener = this
        binding.passwordEditText.onFocusChangeListener = this
        binding.emailEditText.requestFocus()
        binding.passwordEditText.setOnEditorActionListener(this)
    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{
            if (endpoint.equals(APIConstants.LOGIN,true)){
                Utility.setPreferences(this,SPConstants.USER,response)
                StyleableToast.makeText(this, "Logged In Successfully!", Toast.LENGTH_LONG, R.style.mytoast).show();
                startActivity(Intent(this,MainActivity::class.java))
                finish()

            }
            else if (endpoint.equals(APIConstants.CHECK_RIDE_STATUS,true)){
                val json = JSONObject(response)
                if (json.has("order") && json.has("ride_order")) {
                    Utility.setPreferences(this,SPConstants.ORDER,json.getJSONObject("order") .toString())
                    Utility.setPreferences(this,SPConstants.RIDE,json.getJSONObject("ride_order").toString())
                    startActivity(Intent(this, MapActivity::class.java)
                        .putExtra(Constants.RIDER_DATA,json.getJSONObject("ride_order").toString())
                        .putExtra(Constants.ORDER_DATA,json.getJSONObject("order").toString()))
                    finish()
                }
                else{
                    startActivity(Intent(this,MainActivity::class.java))
                    finish()
                }


            }


        }
        catch (e:Exception){
            OnException(endpoint,e)
        }

    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        try {
            if (endpoint.equals(APIConstants.CHECK_RIDE_STATUS, true)) {
                startActivity(Intent(this,MainActivity::class.java))
                finish()


            } else {
                Utility.showErrorDialog(
                    this,
                    R.layout.dialog_error_505,
                    Messages.ERROR_TITLE,
                    message
                )
            }
        }
        catch (e:java.lang.Exception){
            OnException(endpoint,e)
        }
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        if (endpoint.equals(APIConstants.CHECK_RIDE_STATUS, true)) {
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        } else {
            Utility.showErrorDialog(this, R.layout.dialog_error_505, Messages.ERROR_TITLE, message)
        }

    }

    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialog(this,R.layout.dialog_error,Messages.ERROR_TITLE,exception.localizedMessage!!)

    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialog(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            binding.button.id->{
                validateInputs()
            }
        }

    }

    override fun onFocusChange(v: View?, hasFocus: Boolean) {
        if(binding.emailEditText.hasFocus()){
            setActive(true,false)
        }
        else if (binding.passwordEditText.hasFocus()){
            setActive(false,true)
        }

    }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId== EditorInfo.IME_ACTION_DONE){
            validateInputs()
            return true
        }
        return true
    }

    fun setActive(email: Boolean,password: Boolean){
        if (email){
            setGreenDrawable(binding.emailEditText)
            setGrayDrawable(binding.passwordEditText)

        }
        else if (password){
            setGrayDrawable(binding.emailEditText)
            setGreenDrawable(binding.passwordEditText)
        }



    }
    fun setGrayDrawable(editText: EditText){
        editText.background=resources.getDrawable(R.drawable.email_grey,theme)

    }
    fun setGreenDrawable(editText: EditText){
        editText.background=resources.getDrawable(R.drawable.email_green,theme)
    }

    private fun validateInputs() {
        val email= Utility.getTextET(binding.emailEditText)
        val password=Utility.getTextET(binding.passwordEditText)
        if (email.isEmpty()){
            binding.emailEditText.error= Messages.EMPTY_FIELD
        }
        else if (password.isEmpty()){
            binding.passwordEditText.error= Messages.EMPTY_FIELD
        }
        else if (password.length<8){
            binding.passwordEditText.error= Messages.PASSWORD_ERROR
        }
        else if (!Patterns.EMAIL_ADDRESS.matcher(Utility.getTextET(binding.emailEditText)).matches()){
            binding.emailEditText.error= Messages.INVALID_EMAIL
        }
        else{
            list.clear()
            list.add(ModelCommunication(Constants.EMAIL,email))
            list.add(ModelCommunication(Constants.PASSWORD,password))
            list.add(ModelCommunication(SPConstants.FCM,Utility.getFCM(this@LoginActivity)))
            NetworkCall.callAPI(this,getService().login(Utility.getJson(list)),this,true,
                APIConstants.LOGIN)

        }
    }
}