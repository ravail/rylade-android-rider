package com.senarios.ryladerider.activities


import android.content.Intent
import android.content.IntentSender
import android.location.Location
import android.os.Looper
import android.view.View
import androidx.databinding.DataBindingUtil
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import com.senarios.ryladerider.R
import com.senarios.ryladerider.databinding.FragmentMapBinding
import com.senarios.ryladerider.utility.Utility
import kotlinx.android.synthetic.main.toolbar.view.*


abstract class BaseMapActivity : BaseAcivity() {
    private val REQUEST_CHECK_SETTINGS: Int=1000
    private lateinit var locationRequest: LocationRequest
    private lateinit var builder: LocationSettingsRequest.Builder


    public fun createLocationRequest() {
        locationRequest = LocationRequest.create()
        locationRequest.interval = 500
        locationRequest.fastestInterval = 500
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        locationRequestTask()
    }
    private fun locationRequestTask(){
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener {
            locationServiceSuccess()
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException){
                try {
                    exception.startResolutionForResult(this@BaseMapActivity, REQUEST_CHECK_SETTINGS)
                }

                catch (sendEx: IntentSender.SendIntentException) {

                }
            }
        }
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode==REQUEST_CHECK_SETTINGS) {
            if (Utility.checkLocationServices(this)) {
                locationServiceSuccess()
            } else {
                locationRequestTask()
            }
        }
    }



    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (Utility.hasLocationPermission(this)){
            createLocationRequest()
        }
        else{
            handleLocationPermissionResult()
        }

    }

    override fun OnCancelPermissioDialog() {
        finish()
    }

    override fun OnPermissionApproved() {

    }



    abstract fun locationServiceSuccess()






}