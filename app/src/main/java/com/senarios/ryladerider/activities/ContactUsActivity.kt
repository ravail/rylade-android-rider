package com.senarios.ryladerider.activities

import android.util.Log
import android.view.View
import com.google.gson.reflect.TypeToken
import com.senarios.rylad.BaseDialogeFragment
import com.senarios.rylade.costants.Messages
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.rylade.retrofit.ApiResponse
import com.senarios.ryladerider.R
import com.senarios.ryladerider.model.ContactModel
import com.senarios.ryladerider.model.ModelBaseResponse
import com.senarios.ryladerider.retrofit.NetworkCall
import com.senarios.ryladerider.utility.Utility
import mehdi.sakout.aboutpage.AboutPage
import retrofit2.Response
import java.lang.reflect.Type


class ContactUsActivity : BaseAcivity() , ApiResponse {
    private val TAG=javaClass.name


    override fun setBinding() {
//        binding=DataBindingUtil.setContentView(this, R.layout.activity_faq)
    }

    override fun init() {
        NetworkCall.callAPI(this,getService().getContactUS(),this,true, APIConstants.GET_CONTACT_US)
    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnChange(fragment: BaseDialogeFragment) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun OnTrigger() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        val listType: Type = object : TypeToken<List<ContactModel>>() {}.getType()
        val list= Utility.getgson().fromJson<List<ContactModel>>(response, listType)
        val aboutPage: View = AboutPage(this)
            .isRTL(false)
            .setImage(R.drawable.ic_android_black)
            .addGroup("Connect with us")
            .addEmail(list[0].email)
            .addWebsite(list[0].websiteLink)
            .addFacebook(list[0].facebookLink)
            .addInstagram(list[0].instagramLink)
            .create()
        setContentView(aboutPage)
        Log.v(TAG,""+list.size)
    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialog(this@ContactUsActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        Utility.showErrorDialog(this@ContactUsActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialog(this@ContactUsActivity,R.layout.dialog_error, Messages.ERROR_TITLE,getString(R.string.something_went_wrong))
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialog(this@ContactUsActivity)
    }
}