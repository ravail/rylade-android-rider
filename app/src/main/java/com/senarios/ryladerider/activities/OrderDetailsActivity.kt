package com.senarios.ryladerider.activities

import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.divyanshu.draw.activity.DrawingActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.*
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener
import com.google.gson.reflect.TypeToken
import com.muddzdev.styleabletoast.StyleableToast
import com.senarios.rylade.costants.Codes
import com.senarios.rylade.costants.Constants
import com.senarios.rylade.costants.Messages
import com.senarios.rylade.models.MedicineModel
import com.senarios.rylade.models.ParcelModel
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.ryladerider.retrofit.APIConstants.Companion.GET_ADDRESS
import com.senarios.rylade.retrofit.ApiResponse
import com.senarios.ryladerider.R
import com.senarios.ryladerider.adaptors.AdaptorOrderEcommerce
import com.senarios.ryladerider.adaptors.AdaptorOrderMedicine
import com.senarios.ryladerider.adaptors.RecyclerViewCallback
import com.senarios.ryladerider.databinding.FragmentOrderEcommerceDetailsBinding
import com.senarios.ryladerider.databinding.FragmentOrderMedicineDetailsBinding
import com.senarios.ryladerider.databinding.FragmentOrderParcelDetailsBinding
import com.senarios.ryladerider.databinding.FragmentOrderRideDetailsBinding
import com.senarios.ryladerider.model.*
import com.senarios.ryladerider.retrofit.NetworkCall
import com.senarios.ryladerider.utility.Utility
import com.senarios.ryladerider.utility.Utility.Companion.bitmapDescriptorFromVector
import com.senarios.ryladerider.utility.Utility.Companion.loadFullScreenImageView
import com.senarios.ryladerider.utility.Utility.Companion.showLog
import kotlinx.android.synthetic.main.toolbar.view.*
import retrofit2.Response
import java.lang.reflect.Type
import java.text.DecimalFormat


class OrderDetailsActivity : BaseAcivity(), OnMapReadyCallback, OnOffsetChangedListener, ApiResponse, View.OnClickListener, RecyclerViewCallback {
    private lateinit var vParcelbinding: FragmentOrderParcelDetailsBinding
    private lateinit var vMedicineBinding: FragmentOrderMedicineDetailsBinding
    private lateinit var vRideBinding: FragmentOrderRideDetailsBinding
    private lateinit var vEcommerceBinding: FragmentOrderEcommerceDetailsBinding
    private lateinit var ecommerceOrderResponse: BaseEcommerceOrderResponse
    private lateinit var rideModel: RideModel
    private lateinit var order: OrderModel
    private lateinit var baseMedicineResponse: BaseMedicineResponse
    private lateinit var parcelOrderModel: ParcelModel
    private lateinit var medicineList: MutableList<MedicineModel>
    private var bundle: Bundle? = null
    private var scrollRange = -1
    private var markers: MutableList<MarkerOptions> = mutableListOf()
    private val complete_Order_Text = "Complete Order"
    private lateinit var address: Address


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.bundle = savedInstanceState

    }

    override fun setBinding() {
        try{
            val b:Bundle=intent.extras!!
            order = intent.getParcelableExtra(Constants.ORDER_DATA)!!

            showLog(""+order.toString())
            if (::order.isInitialized && order != null) {
                callOrderDetailAPI()
            }
        }
        catch (e:java.lang.Exception){
            e.printStackTrace()
        }

    }

    private fun callOrderDetailAPI() {
        val list: MutableList<ModelCommunication> = mutableListOf()
        list.add(ModelCommunication(Constants.ORDER_ID, order.id.toString()))
        list.add(ModelCommunication(Constants.VENDOR_ID,order.vendor_id.toString()))
        NetworkCall.callAPI(this, getService().getOrderDetails(APIConstants.BEARER + Utility.getUser(this).token, Utility.getJson(list)), this, true, APIConstants.GET_ORDER_DETAIl)
    }

    private fun initMedicineDetails() {
        vMedicineBinding.mapView.onCreate(bundle)
        vMedicineBinding.mapView.getMapAsync(this)
        vMedicineBinding.order = order
        vMedicineBinding.data=medicineList[0]

        vMedicineBinding.user=baseMedicineResponse.user
        vMedicineBinding.setAddress(baseMedicineResponse.address!!)
        vMedicineBinding.toolbar.setOnClickListener(this)
        vMedicineBinding.accept.setOnClickListener(this)
        vMedicineBinding.reject.setOnClickListener(this)
        vMedicineBinding.changeStatusBtn.setOnClickListener(this)

        vMedicineBinding.imageView.setOnClickListener { loadFullScreenImageView(this@OrderDetailsActivity,APIConstants.BASE_MEDICINE_IMAGE+medicineList[0].photo) }

        vMedicineBinding.recyclerView.adapter = AdaptorOrderMedicine(this, medicineList, this)
        if (order.status!!.equals(Constants.ORDER_STATUS_PENDING, true) || order.status!!.equals(Constants.ORDER_STATUS_APPROVED, true)) {
            vMedicineBinding.accept.visibility = View.VISIBLE
            vMedicineBinding.reject.visibility = View.VISIBLE
        } else {
            vMedicineBinding.accept.visibility = View.GONE
            vMedicineBinding.reject.visibility = View.GONE

        }
        if (order.status.equals(Constants.ORDER_STATUS_ASSIGNED, true)) {
            vMedicineBinding.changeStatusBtn.visibility = View.VISIBLE
            vMedicineBinding.changeStatusBtn.text = complete_Order_Text
        } else if (order.status.equals(Constants.ORDER_STATUS_CANCELLED)) {
            vMedicineBinding.changeStatusBtn.visibility = View.GONE
        } else if (order.status.equals(Constants.ORDER_STATUS_COMPLETED)) {
            vMedicineBinding.changeStatusBtn.visibility = View.GONE
        }
    }

    private fun initParcelDetails() {
        vParcelbinding.mapView.onCreate(bundle)
        vParcelbinding.mapView.getMapAsync(this)
        vParcelbinding.order = order
        vParcelbinding.data = parcelOrderModel
        vParcelbinding.collapseActionView.title = "Parcel Delivery"
        vParcelbinding.toolbar.setNavigationOnClickListener(this)
        vParcelbinding.appbar.addOnOffsetChangedListener(this)
        vParcelbinding.imageview.setOnClickListener(this)
        vParcelbinding.accept.setOnClickListener(this)
        vParcelbinding.reject.setOnClickListener(this)
        vParcelbinding.changeStatusBtn.setOnClickListener(this)
        if (order.status!!.equals(Constants.ORDER_STATUS_PENDING, true) || order.status!!.equals(Constants.ORDER_STATUS_APPROVED, true)) {
            vParcelbinding.groupId.visibility = View.VISIBLE
        } else {
            vParcelbinding.groupId.visibility = View.GONE
        }

        if (order.status.equals(Constants.ORDER_STATUS_ASSIGNED, true)) {
            vParcelbinding.changeStatusBtn.visibility = View.VISIBLE
            vParcelbinding.changeStatusBtn.text = complete_Order_Text
        } else if (order.status.equals(Constants.ORDER_STATUS_CANCELLED)) {
            vParcelbinding.changeStatusBtn.visibility = View.GONE
        } else if (order.status.equals(Constants.ORDER_STATUS_COMPLETED)) {
            vParcelbinding.changeStatusBtn.visibility = View.GONE
        }
    }

    private fun initRideDetails() {
        vRideBinding.toolbar.setNavigationOnClickListener(this)
        vRideBinding.mapView.onCreate(bundle)
        vRideBinding.mapView.getMapAsync(this)
        vRideBinding.accept.setOnClickListener(this)
        vRideBinding.reject.setOnClickListener(this)
        vRideBinding.changeStatusBtn.setOnClickListener(this)

        if (order.status!!.equals(Constants.ORDER_STATUS_PENDING, true) || order.status!!.equals(Constants.ORDER_STATUS_APPROVED, true)) {
            vRideBinding.groupId.visibility = View.VISIBLE
        } else {
            vRideBinding.groupId.visibility = View.GONE
        }
        if (order.status.equals(Constants.ORDER_STATUS_CANCELLED)) {
            vRideBinding.changeStatusBtn.visibility = View.GONE
        } else if (order.status.equals(Constants.ORDER_STATUS_COMPLETED)) {
            vRideBinding.changeStatusBtn.visibility = View.GONE
        }
    }

    private fun initEcommerceDetails() {
        vEcommerceBinding.mapView.onCreate(bundle)
        vEcommerceBinding.mapView.getMapAsync(this)
        vEcommerceBinding.order = order
        vEcommerceBinding.address = ecommerceOrderResponse.address
        vEcommerceBinding.user=ecommerceOrderResponse.user
        vEcommerceBinding.vendor=ecommerceOrderResponse.vendor
        vEcommerceBinding.product = ecommerceOrderResponse.list[0]
        vEcommerceBinding.toolbar.toolbar.setNavigationOnClickListener(this)
        vEcommerceBinding.toolbar.toolbar.title = "Ecommerce Order Details"
        vEcommerceBinding.accept.setOnClickListener(this)
        vEcommerceBinding.reject.setOnClickListener(this)
        vEcommerceBinding.changeStatusBtn.setOnClickListener(this)
        vEcommerceBinding.recyclerView.adapter = AdaptorOrderEcommerce(this, ecommerceOrderResponse.list, this)

        if (order.status!!.equals(Constants.ORDER_STATUS_PENDING, true) || order.status!!.equals(Constants.ORDER_STATUS_APPROVED, true)) {
            vEcommerceBinding.groupId.visibility = View.VISIBLE
        } else {
            vEcommerceBinding.groupId.visibility = View.GONE
        }
        if (order.status.equals(Constants.ORDER_STATUS_CANCELLED)) {
            vEcommerceBinding.changeStatusBtn.visibility = View.GONE
        }
        if (order.status.equals(Constants.ORDER_STATUS_ASSIGNED, true)) {
            vEcommerceBinding.changeStatusBtn.visibility = View.VISIBLE
            vEcommerceBinding.changeStatusBtn.text = complete_Order_Text
        } else if (order.status.equals(Constants.ORDER_STATUS_COMPLETED)) {
            vEcommerceBinding.changeStatusBtn.visibility = View.GONE
        }

    }

    override fun init() {

    }

    private fun isEcom(): Boolean {
        return ::vEcommerceBinding.isInitialized
    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        if (scrollRange == -1) {
            scrollRange = appBarLayout!!.totalScrollRange
        }
        if (scrollRange + verticalOffset == 0) {
            vParcelbinding.toolbar.setBackgroundColor(ContextCompat.getColor(this@OrderDetailsActivity, R.color.white))
            vParcelbinding.toolbar.setNavigationIcon(R.drawable.close)
        } else {
            vParcelbinding.toolbar.title = "Parcel Delivery"
            vParcelbinding.toolbar.setNavigationIcon(R.drawable.ic_close)
            vParcelbinding.toolbar.setBackgroundColor(ContextCompat.getColor(this@OrderDetailsActivity, R.color.transparent))
        }
    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try {

            if (endpoint.equals(APIConstants.GET_ORDER_DETAIl)) {
                initViews(response)
            } else if (endpoint.equals(APIConstants.ACCEPT_ORDER)) {
                acceptOrderFlow(response)
            } else if (endpoint.equals(APIConstants.REJECT_ORDER)) {
                rejectOrderFlow(response)
            } else if (endpoint.equals(APIConstants.CHANGE_ORDER_STATUS)) {
                statusChangeFlow(response)
            } else if (endpoint.equals(APIConstants.COMPLETE_ORDER)) {
                completeOrder()
            } else if (endpoint.equals(GET_ADDRESS)) {
                address = Utility.getgson().fromJson(response, Address::class.java)
                initMedicineDetails()
            }


        } catch (e: Exception) {
            OnException(endpoint, e)
        }

    }

    private fun completeOrder() {
        Utility.showNotification(this,"Order Completed","Please Collect "+order.amount+" PKR" + " from customer")
        Utility.getAlertDialoge(this,"Order Completed","Please Collect "+order.amount+" PKR" + " from customer")
            .setPositiveButton("Ok") { dialog, which ->
                finish()
                StyleableToast.makeText(this@OrderDetailsActivity, "Order Completed Successfully!", Toast.LENGTH_LONG, R.style.mytoast).show();
            }
            .show()


    }

    private fun takeSignature() {
        if (Utility.hasMediaPermission(this)) {
            val intent = Intent(this, DrawingActivity::class.java)
            startActivityForResult(intent, Codes.REQUEST_CODE_DRAW)
        } else {
            checkMediaPermission()
        }
    }

    private fun statusChangeFlow(response: String) {
        order = Utility.getgson().fromJson(response, OrderModel::class.java)
        if (order.status.equals(Constants.ORDER_STATUS_COMPLETED, true)) {
            completeOrder()
        }
    }

    private fun rejectOrderFlow(response: String) {
        try {
            finish()
            StyleableToast.makeText(this, "Order Rejected Successfully!", Toast.LENGTH_LONG, R.style.mytoast).show();
        } catch (e: Exception) {
            OnException("", e)
        }
    }

    private fun acceptOrderFlow(response: String) {
        try {
            order = Utility.getgson().fromJson(response, OrderModel::class.java)
            if (order.status.equals(Constants.ORDER_STATUS_ASSIGNED, true)) {
                when (order.order_type) {
                    1 -> {
                        vMedicineBinding.accept.visibility = View.GONE
                        vMedicineBinding.reject.visibility = View.GONE
                        vMedicineBinding.changeStatusBtn.visibility = View.VISIBLE
                        vMedicineBinding.changeStatusBtn.text = complete_Order_Text
                    }
                    2 -> {
                        vParcelbinding.changeStatusBtn.visibility = View.VISIBLE
                        vParcelbinding.groupId.visibility = View.GONE
                        vParcelbinding.changeStatusBtn.text = complete_Order_Text
                    }
                    3 -> {
                        callOrderDetailAPI()
                    }
                    4 -> {
                        vEcommerceBinding.changeStatusBtn.visibility = View.VISIBLE
                        vEcommerceBinding.groupId.visibility = View.GONE
                        vEcommerceBinding.changeStatusBtn.text = complete_Order_Text
                    }

                }

            }
        } catch (e: Exception) {
            OnException("", e)
        }


    }

    private fun initViews(response: String) {
        when (order.order_type) {
            1 -> {
//                val listType: Type = object : TypeToken<List<MedicineModel?>?>() {}.getType()
//                medicineList = Utility.getgson().fromJson(response, listType)
                baseMedicineResponse=Utility.getgson().fromJson<BaseMedicineResponse>(response,BaseMedicineResponse::class.java)
                medicineList=baseMedicineResponse.items!!
                address=baseMedicineResponse.address!!
                vMedicineBinding = DataBindingUtil.setContentView(this, R.layout.fragment_order_medicine_details)
                if (isAddress()) {
                    initMedicineDetails()
                }
//                } else {
//                    callgetAddressAPI()
//                }
            }
            2 -> {
                parcelOrderModel = Utility.getgson().fromJson(response, ParcelModel::class.java)
                vParcelbinding = DataBindingUtil.setContentView(this, R.layout.fragment_order_parcel_details)
                initParcelDetails()
            }
            3 -> {
                rideModel = Utility.getgson().fromJson(response, RideModel::class.java)
                vRideBinding = DataBindingUtil.setContentView(this, R.layout.fragment_order_ride_details)
                initRideDetails()
                if (order.status.equals(Constants.ORDER_STATUS_ASSIGNED)) {
                    startActivity(Intent(this@OrderDetailsActivity, MapActivity::class.java).putExtra(Constants.RIDER_DATA, Utility.getgson().toJson(rideModel)).putExtra(Constants.ORDER_DATA,
                            Utility.getgson().toJson(order)))
                    finish()
                }
            }
            4 -> {
                ecommerceOrderResponse = Utility.getgson().fromJson(response, BaseEcommerceOrderResponse::class.java)
                vEcommerceBinding = DataBindingUtil.setContentView(this, R.layout.fragment_order_ecommerce_details)
                initEcommerceDetails()
            }
            else -> {

            }
        }
    }

    private fun callgetAddressAPI() {
        val list: MutableList<ModelCommunication> = mutableListOf()
        list.add(ModelCommunication(Constants.ADDRESS_ID, order.address_id.toString()))
        NetworkCall.callAPI(this, Utility.getService().getAddress(APIConstants.BEARER + Utility.getUser(this).token, Utility.getJson(list)), this, true, APIConstants.GET_ADDRESS)

    }

    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialogBuilder(this@OrderDetailsActivity, R.layout.dialog_error_505, Messages.ERROR_TITLE, message).setPositiveButton("OK", { dialog, which -> finish() }).show()
    }

    override fun OnError(endpoint: String, code: Int, message: String) {
        Utility.showErrorDialogBuilder(this@OrderDetailsActivity, R.layout.dialog_error_505, Messages.ERROR_TITLE, message).setPositiveButton("OK", { dialog, which -> finish() }).show()
    }

    override fun OnException(endpoint: String, exception: Throwable) {
        Utility.showErrorDialogBuilder(this@OrderDetailsActivity, R.layout.dialog_error, Messages.ERROR_TITLE, getString(R.string.something_went_wrong))
            .setPositiveButton("OK", { dialog, which -> finish() }).show()

    }

    override fun OnNetworkError(endpoint: String, message: String) {
        Utility.showNErrorDialogwithFinish(this@OrderDetailsActivity)
    }

    override fun onClick(v: View?) {
        val id = v?.id
        if (isParcel() && id == vParcelbinding.imageview.id) {
            loadFullScreenImageView(this, APIConstants.BASE_PARCEL_IMAGE + parcelOrderModel.photo)
        } else if (isParcel() && id == vParcelbinding.accept.id) {
            acceptOrderAPI()
        } else if (isParcel() && id == vParcelbinding.reject.id) {
            rejectOrderAPI()
        } else if (isMedicine() && id == vMedicineBinding.accept.id) {
            acceptOrderAPI()
        } else if (isMedicine() && id == vMedicineBinding.reject.id) {
            rejectOrderAPI()
        } else if (isRide() && id == vRideBinding.accept.id) {
            acceptOrderAPI()
        } else if (isRide() && id == vRideBinding.reject.id) {
            rejectOrderAPI()
        } else if (isMedicine() && id == vMedicineBinding.changeStatusBtn.id) {
            takeSignature()
        } else if (isParcel() && id == vParcelbinding.changeStatusBtn.id) {
            takeSignature()
        } else if (isRide() && id == vRideBinding.changeStatusBtn.id) {

        } else if (isEcom() && id == vEcommerceBinding.changeStatusBtn.id) {
            takeSignature()
        } else if (isEcom() && id == vEcommerceBinding.accept.id) {
            acceptOrderAPI()
        } else if (isEcom() && id == vEcommerceBinding.reject.id) {
            rejectOrderAPI()
        } else {
            finish()
        }


    }

    private fun callChangeStatusApi(bitmap: Bitmap) {
        val file = Utility.getFile(bitmap)
        val list: MutableList<ModelCommunication> = mutableListOf()
        list.add(ModelCommunication(Constants.ORDER_ID, order.id.toString()))
        list.add(ModelCommunication(Constants.STATUS, Constants.ORDER_STATUS_COMPLETED))
        NetworkCall.callAPI(this,
            Utility.getService().completeOrder(APIConstants.BEARER + Utility.getUser(this).token, getPart(file!!.absolutePath, "signature"), order.id.toString(), Constants.ORDER_STATUS_COMPLETED),
            this,
            true,
            APIConstants.CHANGE_ORDER_STATUS)

    }

    private fun rejectOrderAPI() {
        val list: MutableList<ModelCommunication> = mutableListOf()
        list.add(ModelCommunication(Constants.RIDER_ID, Utility.getUser(this).id.toString()))
        list.add(ModelCommunication(Constants.ORDER_ID, order.id.toString()))
        NetworkCall.callAPI(this, Utility.getService().rejectOrder(APIConstants.BEARER + Utility.getUser(this).token, Utility.getJson(list)), this, true, APIConstants.REJECT_ORDER)

    }

    private fun acceptOrderAPI() {
        val list: MutableList<ModelCommunication> = mutableListOf()
        list.add(ModelCommunication(Constants.RIDER_ID, Utility.getUser(this).id.toString()))
        list.add(ModelCommunication(Constants.ORDER_ID, order.id.toString()))
        NetworkCall.callAPI(this, Utility.getService().acceptOrder(APIConstants.BEARER + Utility.getUser(this).token, Utility.getJson(list)), this, true, APIConstants.ACCEPT_ORDER)

    }

    override fun OnClick(position: Int, any: Any) {
        if (any is MedicineModel) {
            loadFullScreenImageView(this, APIConstants.BASE_MEDICINE_IMAGE + (any as MedicineModel).photo)
        } else if (any is EcommerceProductModel) {
            loadFullScreenImageView(this, APIConstants.BASE_PRODUCT_PHOTO + (any as EcommerceProductModel).image)
        }

    }

    private fun isParcel(): Boolean {
        return ::vParcelbinding.isInitialized
    }

    private fun isMedicine(): Boolean {
        return ::vMedicineBinding.isInitialized
    }

    private fun isRide(): Boolean {
        return ::vRideBinding.isInitialized
    }

    private fun moveCamera(latlng: LatLng, map: GoogleMap) {
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, 18f))
    }

    override fun onMapReady(p0: GoogleMap?) {
        if (p0 != null) {

            p0.uiSettings.isScrollGesturesEnabled = true
            p0.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style))
            try {
                if (isMedicine()) {
                    p0.addMarker(MarkerOptions().icon(bitmapDescriptorFromVector(this, R.drawable.ic_dropoff_location)).title("Medicine Drop Off").position(LatLng(address.lat.toDouble(),
                            address.lon.toDouble()))

                    )
                    vMedicineBinding.mapView.onResume()
                    moveCamera(LatLng(address.lat.toDouble(), address.lon.toDouble()), p0)


                } else if (isParcel()) {
                    p0.addMarker(MarkerOptions().icon(bitmapDescriptorFromVector(this,
                        R.drawable.ic_dropoff_location)).title("Parcel Dropoff").position(LatLng(parcelOrderModel.sender_lat!!.toDouble(), parcelOrderModel.sender_lon!!.toDouble())))
                    p0.addMarker(MarkerOptions().icon(bitmapDescriptorFromVector(this,
                        R.drawable.ic_pickup_location)).title("Parcel Pick Up").position(LatLng(parcelOrderModel.receiver_lat!!.toDouble(), parcelOrderModel.receiver_lon!!.toDouble())))
                    moveCamera(getBounds(LatLng(parcelOrderModel.receiver_lat!!.toDouble(), parcelOrderModel.receiver_lon!!.toDouble()),
                        LatLng(parcelOrderModel.sender_lat!!.toDouble(), parcelOrderModel.sender_lon!!.toDouble())), p0)
                    vParcelbinding.mapView.onResume()

                } else if (isRide()) {
                    p0.uiSettings.isMyLocationButtonEnabled = true
                    MapsInitializer.initialize(this)
                    p0.addMarker(MarkerOptions().icon(bitmapDescriptorFromVector(this, R.drawable.ic_dropoff_location)).title("Drop Off").position(LatLng(rideModel.dropoff_lat!!.toDouble(),
                            rideModel.dropoff_lon!!.toDouble())))
                    p0.addMarker(MarkerOptions().icon(bitmapDescriptorFromVector(this, R.drawable.ic_pickup_location)).title("Pick Up").position(LatLng(rideModel.pickup_lat!!.toDouble(),
                            rideModel.pickup_lon!!.toDouble())))
                    moveCamera(getBounds(LatLng(rideModel.dropoff_lat!!.toDouble(), rideModel.dropoff_lon!!.toDouble()), LatLng(rideModel.pickup_lat!!.toDouble(), rideModel.pickup_lon!!.toDouble())),
                        p0)
                    rideModel.distance = DecimalFormat("##.##").format(rideModel.distance!!.toDouble())
                    vRideBinding.data = rideModel
                    vRideBinding.mapView.onResume()
                } else if (isEcom()) {
                    p0.uiSettings.isMyLocationButtonEnabled = true
                    MapsInitializer.initialize(this)
                    p0.addMarker(MarkerOptions().icon(bitmapDescriptorFromVector(this, R.drawable.ic_dropoff_location)).title("User Address").position(LatLng(ecommerceOrderResponse.address.lat.toDouble(),
                        ecommerceOrderResponse.address.lon.toDouble())))
                    p0.addMarker(MarkerOptions().icon(bitmapDescriptorFromVector(this, R.drawable.ic_pickup_location)).title("Vendor Address").position(LatLng(ecommerceOrderResponse.vendor.lat_vendor!!.toDouble(),
                        ecommerceOrderResponse.vendor.long_vendor!!.toDouble())))
                    moveCamera(getBounds(LatLng(ecommerceOrderResponse.address.lat.toDouble(),
                        ecommerceOrderResponse.address.lon.toDouble()), LatLng(ecommerceOrderResponse.vendor.lat_vendor!!.toDouble(),
                        ecommerceOrderResponse.vendor.long_vendor!!.toDouble())),
                        p0)
                    vEcommerceBinding.mapView.onResume()
                }

            } catch (e: Exception) {
//                OnException("", e)
                Utility.showLog("Error in order detail"+e.localizedMessage)
            }
        }
    }

    private fun moveCamera(bounds: LatLngBounds, map: GoogleMap) {
        map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
    }

    private fun getBounds(latlng1: LatLng, latlng2: LatLng): LatLngBounds {
        val builder = LatLngBounds.Builder()
        builder.include(latlng1)
        builder.include(latlng2)
        return builder.build()
    }

    override fun onStart() {
        super.onStart()
        if (isRide()) {
            vRideBinding.mapView.onStart()
        } else if (isMedicine()) {
            vMedicineBinding.mapView.onStart()
        } else if (isParcel()) {
            vParcelbinding.mapView.onStart()
        }
    }

    override fun onResume() {
        super.onResume()
        if (isRide()) {
            vRideBinding.mapView.onResume()
        } else if (isMedicine()) {
            vMedicineBinding.mapView.onResume()
        } else if (isParcel()) {
            vParcelbinding.mapView.onResume()
        }
    }

    override fun onStop() {
        super.onStop()
        if (isRide()) {
            vRideBinding.mapView.onStop()
        } else if (isMedicine()) {
                vMedicineBinding.mapView.onStop()
        } else if (isParcel()) {
            vParcelbinding.mapView.onStop()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (isRide()) {
            vRideBinding.mapView.onDestroy()
        } else if (isMedicine()) {
            vMedicineBinding.mapView.onDestroy()
        } else if (isParcel()) {
            vParcelbinding.mapView.onDestroy()
        }
    }

    override fun onLowMemory() {
        super.onLowMemory()
        if (isRide()) {
            vRideBinding.mapView.onLowMemory()
        } else if (isMedicine()) {
            vMedicineBinding.mapView.onLowMemory()
        } else if (isParcel()) {
            vParcelbinding.mapView.onLowMemory()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Codes.REQUEST_CODE_DRAW && data != null) {
            val result = data.getByteArrayExtra("bitmap")
            if (result != null) {
                val bitmap = BitmapFactory.decodeByteArray(result, 0, result.size)
                callChangeStatusApi(bitmap)
            }
        }
    }

    private fun isAddress(): Boolean {
        return ::address.isInitialized
    }
}