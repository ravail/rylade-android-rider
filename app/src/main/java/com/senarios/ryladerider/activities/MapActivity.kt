package com.senarios.ryladerider.activities


import android.content.Intent
import android.location.Location
import android.os.SystemClock
import android.view.View
import android.widget.Chronometer
import androidx.databinding.DataBindingUtil
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.senarios.rylade.costants.Constants
import com.senarios.rylade.costants.SPConstants
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.rylade.retrofit.ApiResponse
import com.senarios.ryladerider.R
import com.senarios.ryladerider.databinding.FragmentMapBinding
import com.senarios.ryladerider.model.*
import com.senarios.ryladerider.retrofit.NetworkCall
import com.senarios.ryladerider.service.BackgroundLocation
import com.senarios.ryladerider.utility.Utility
import com.senarios.ryladerider.utility.Utility.Companion.bitmapDescriptorFromVector
import com.senarios.ryladerider.utility.Utility.Companion.getJson
import com.senarios.ryladerider.utility.Utility.Companion.showLog
import kotlinx.android.synthetic.main.toolbar.view.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import retrofit2.Response
import java.lang.Exception
import java.text.DecimalFormat
import kotlin.math.roundToInt


class MapActivity : BaseMapActivity(), View.OnClickListener, ApiResponse, Chronometer.OnChronometerTickListener, OnMapReadyCallback {
    private lateinit var binding: FragmentMapBinding
    private lateinit var map: GoogleMap
    private lateinit var location: Location
    private val currentZoom: Float = 18f
    private lateinit var rideModel: RideModel
    private lateinit var orderModel: OrderModel
    private val results: FloatArray = FloatArray(1)
    private val list: MutableList<ModelCommunication> = mutableListOf()

    override fun locationServiceSuccess() {
        initMap()
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun setBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.fragment_map)
    }

    override fun onMapReady(p0: GoogleMap?) {
        if (p0 != null) {
            rideModel = Utility.getgson().fromJson(intent.getStringExtra(Constants.RIDER_DATA), RideModel::class.java)
            orderModel = Utility.getgson().fromJson(intent.getStringExtra(Constants.ORDER_DATA), OrderModel::class.java)
            map = p0;
            map.addMarker(MarkerOptions().position(LatLng(rideModel.dropoff_lat!!.toDouble(), rideModel.dropoff_lon!!.toDouble())).icon(bitmapDescriptorFromVector(this,
                R.drawable.ic_dropoff_location)).title("Drop Off"))
            map.addMarker(MarkerOptions().position(LatLng(rideModel.pickup_lat!!.toDouble(), rideModel.pickup_lon!!.toDouble())).icon(bitmapDescriptorFromVector(this,
                R.drawable.ic_pickup_location)).title("Pick up"))
            moveCamera(getBounds(LatLng(rideModel.dropoff_lat!!.toDouble(), rideModel.dropoff_lon!!.toDouble()), LatLng(rideModel.pickup_lat!!.toDouble(), rideModel.pickup_lon!!.toDouble())), p0)
            setButton()

        }
    }

    override fun OnPermissionApproved() {
        super.OnPermissionApproved()
        if (!isLocation()) {
            if (!(Utility.getPreferences(this, SPConstants.IS_RIDE, true) as Boolean)) {
                startService(Intent(this@MapActivity, BackgroundLocation::class.java))
            }
        }
    }

    private fun setButton() {
        binding.startBtn.setOnClickListener(this)
        binding.startBtn.visibility = View.VISIBLE
        if (rideModel.status == null) {
            changeButton(R.drawable.basic_button, Constants.ON_SITE)
        } else if (rideModel.status.equals(Constants.ON_SITE, true)) {
            changeButton(R.drawable.basic_button, Constants.START_RIDE)
            binding.setFare(Fare(0, "0"))
            binding.inRideView.visibility = View.VISIBLE
        } else if (rideModel.status.equals(Constants.START_RIDE, true)) {
            binding.inRideView.visibility = View.VISIBLE
            changeButton(R.drawable.red_button, Constants.END_RIDE)
        } else if (rideModel.status.equals(Constants.END_RIDE, true)) {
            binding.inRideView.visibility = View.VISIBLE
            changeButton(R.drawable.red_button, Constants.END_RIDE)
        }

        if (Utility.getPreference(this).contains(SPConstants.INITIAL_FARE)) {
            restoreRideState()
        }
    }

    private fun moveCamera(bounds: LatLngBounds, map: GoogleMap) {
        map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
    }

    private fun getBounds(latlng1: LatLng, latlng2: LatLng): LatLngBounds {
        val builder = LatLngBounds.Builder()
        builder.include(latlng1)
        builder.include(latlng2)
        return builder.build()
    }


    override fun init() {
        binding.toolbar.toolbar.setNavigationOnClickListener(this)
        binding.chronometer.onChronometerTickListener = this
        if (!Utility.hasLocationPermission(this)) {
            checkLocationPermission()

        } else {
            createLocationRequest()
            if (!isLocation()) {
                if (!(Utility.getPreferences(this, SPConstants.IS_RIDE, true) as Boolean)) {
                    startService(Intent(this@MapActivity, BackgroundLocation::class.java))
                }
            }
        }


    }

    override fun onResume() {
        super.onResume()
        if (isMap()) {
            if (Utility.getPreference(this).contains(SPConstants.INITIAL_FARE)) {
                restoreRideState()
            }
        }
    }

    public fun moveCamera(latlng: LatLng) {
        if (isMap()) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(latlng, currentZoom))
        }
    }

    public fun isLocation(): Boolean {
        return ::location.isInitialized
    }

    public fun isMap(): Boolean {
        return ::map.isInitialized
    }


    public fun getCurrentLatLng(): LatLng {
        return LatLng(location.latitude, location.longitude)
    }

    @Subscribe(threadMode = ThreadMode.MAIN) fun getLocation(location: Location) {
        this.location = location
    }

    fun isOrder(): Boolean {
        return ::rideModel.isInitialized
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this);
        if (Utility.getPreferences(this, SPConstants.IS_RIDE, true) as Boolean) {
            Utility.setPreferences(this, SPConstants.CHRONO_TIMER, "" + (SystemClock.elapsedRealtime() - binding.chronometer.base))
        }

    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN) fun calculations(fare: Fare) {
        binding.setFare(fare)
        if (Utility.getPreferences(this, SPConstants.IS_RIDE, true) as Boolean) {
            Utility.setPreferences(this, SPConstants.CHRONO_TIMER, "" + (SystemClock.elapsedRealtime() - binding.chronometer.base))
        }
    }

    private fun atLocation(): Boolean {
        try {
            Location.distanceBetween(rideModel.pickup_lat!!.toDouble(), rideModel.pickup_lon!!.toDouble(), location.latitude, location.longitude, results)
            if (results[0].roundToInt() == 100 || results[0].roundToInt() < 100) {
                return true
            }
            return false
        } catch (e: Exception) {
            return false
        }
    }

    override fun onChronometerTick(chronometer: Chronometer?) {


    }

    private fun changeButton(resource: Int, text: String) {
        binding.startBtn.text = text
        binding.startBtn.setBackgroundResource(resource)
    }

    private fun restoreRideState() {
        try {
            val difference = System.currentTimeMillis() - Utility.getPreferences(this, SPConstants.START_TIME_LONG, "").toString().toLong()
            binding.inRideView.visibility = View.VISIBLE
            binding.chronometer.base = SystemClock.elapsedRealtime() - difference
            binding.chronometer.start()
            var lastfare = Utility.getPreferences(this, SPConstants.LAST_FARE, "").toString()
            var initialfare = Utility.getPreferences(this, SPConstants.INITIAL_FARE, "").toString()
            if (lastfare.isEmpty()) {
                lastfare = "0.0"
            }
            if (initialfare.isEmpty()) {
                initialfare = "0.0"
            }
            val fare = initialfare.toDouble() + lastfare.toDouble()
            var distance = Utility.getPreferences(this, SPConstants.TOTAL_M, "").toString()
            if (distance.isEmpty()) {
                distance = "0.0"
            }
            distance = "" + (DecimalFormat("##.##").format(distance.toDouble().roundToInt() / 1000.0))
            binding.setFare(Fare(fare.roundToInt(), distance))
        } catch (e: Exception) {
            showLog("" + e.message)
        }

    }

    override fun onClick(v: View?) {
        when (v?.id) {

            binding.startBtn.id -> {
                if (binding.startBtn.text.equals(Constants.ON_SITE)) {
                    //                    if (atLocation()) {
                    changeRideStatusApi(Constants.ON_SITE)
                    //                    }
                    //                    else{
                    //                        Utility.showToast(this,"You Need to be at user's location to start ride")
                    //                    }
                } else if (binding.startBtn.text.equals(Constants.START_RIDE)) {
                    changeRideStatusApi(Constants.START_RIDE)
                } else if (binding.startBtn.text.equals(Constants.END_RIDE)) {
                    endRide()
                }
            }
            else -> {
                onBackPressed()
            }

        }

    }


    private fun changeRideStatusApi(status:String){
        list.clear()
        list.add(ModelCommunication(Constants.STATUS, status))
        list.add(ModelCommunication(Constants.USER_ID, Utility.getUser(this).id.toString()))
        list.add(ModelCommunication(Constants.ORDER_ID, orderModel.id.toString()))
        NetworkCall.callAPI(this, getService().changeRideStatus(APIConstants.BEARER + Utility.getUser(this).token, getJson(list)), this, true, APIConstants.CHANGE_RIDE_STATUS)

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        if (endpoint.equals(APIConstants.CHANGE_RIDE_STATUS)) {
            if (binding.startBtn.text.equals(Constants.ON_SITE)) {
                OnArrived()
            } else if (binding.startBtn.text.equals(Constants.START_RIDE)) {
                OnRideStarted()
            } else if (binding.startBtn.text.equals(Constants.END_RIDE)) {
                OnRideEnded()
            } else {
                binding.startBtn.visibility = View.GONE
            }

        }

    }
    private fun OnArrived(){
        binding.inRideView.visibility = View.VISIBLE
        binding.setFare(Fare(0, "0"))
        changeButton(R.drawable.basic_button, Constants.START_RIDE)
        EventBus.getDefault().post(ModelCommunication(Constants.ON_SITE, ""))
    }

    private fun OnRideStarted(){
        Utility.setPreferences(this, SPConstants.START_TIME_LONG, "" + System.currentTimeMillis())
        binding.chronometer.base = SystemClock.elapsedRealtime()
        binding.chronometer.start()
        startService(Intent(this@MapActivity, BackgroundLocation::class.java).putExtra(Constants.RIDER_DATA, Utility.getgson().toJson(rideModel)).putExtra(Constants.ORDER_DATA,
            Utility.getgson().toJson(orderModel)))
        changeButton(R.drawable.red_button, Constants.END_RIDE)
        binding.inRideView.visibility = View.VISIBLE
        EventBus.getDefault().post(ModelCommunication(Constants.STOP_TIMER, ""))
    }

    private fun OnRideEnded(){
        binding.chronometer.stop()
        binding.startBtn.visibility = View.GONE
        var distance = Utility.getPreferences(this, SPConstants.TOTAL_M, "").toString()
        var initialFare= Utility.getPreferences(this, SPConstants.INITIAL_FARE, "").toString()
        var calculatedFare=Utility.getPreferences(this, SPConstants.LAST_FARE, "").toString()
        if (distance.isEmpty()) {
            distance = "0.0"
        }
        if (initialFare.isEmpty()){
            initialFare="0.0"
        }
        if (calculatedFare.isEmpty()){
            calculatedFare="0.0"
        }
        distance = "" + (DecimalFormat("##.##").format(distance.toDouble().roundToInt() / 1000.0))
        calculations(Fare((initialFare.toDouble()+calculatedFare.toDouble()).roundToInt(),distance))
        EventBus.getDefault().post(ModelCommunication(Constants.END_RIDE, ""))
    }


    override fun OnStatusfalse(endpoint: String, t: Response<ModelBaseResponse>, response: String, message: String) {

    }

    override fun OnError(endpoint: String, code: Int, message: String) {

    }

    override fun OnException(endpoint: String, exception: Throwable) {

    }

    override fun OnNetworkError(endpoint: String, message: String) {

    }

    private fun endRide() {
        try {
            list.clear()
            var distance = Utility.getPreferences(this, SPConstants.TOTAL_M, "").toString()
            var initialFare= Utility.getPreferences(this, SPConstants.INITIAL_FARE, "").toString()
            var calculatedFare=Utility.getPreferences(this, SPConstants.LAST_FARE, "").toString()
            if (distance.isEmpty()) {
                distance = "0.0"
            }
            if (initialFare.isEmpty()){
                initialFare="0.0"
            }
            if (calculatedFare.isEmpty()){
                calculatedFare="0.0"
            }
            distance = "" + (DecimalFormat("##.##").format(distance.toDouble().roundToInt() / 1000.0))
            val lastTickLocation = Utility.getgson().fromJson<Location>(Utility.getPreferences(this, SPConstants.LAST_LAT_LNG, "").toString(), Location::class.java)
            list.add(ModelCommunication(Constants.STATUS, Constants.END_RIDE))
            list.add(ModelCommunication(Constants.USER_ID, Utility.getUser(this).id.toString()))
            list.add(ModelCommunication(Constants.ORDER_ID, orderModel.id.toString()))
            list.add(ModelCommunication(Constants.INITIAL_FARE, ""+ initialFare.toDouble().roundToInt()))
            list.add(ModelCommunication(Constants.CALCULATED_FARE, "" + calculatedFare.toDouble().roundToInt()))
            list.add(ModelCommunication(Constants.WAITING_TIME,""+ (Utility.getPreferences(this, SPConstants.TIMER, 0).toString().toInt() / 60.0).roundToInt()))
            list.add(ModelCommunication(Constants.WAITING_COST, "" + (Utility.getPreferences(this, SPConstants.TIMER, 0).toString().toInt() / 60.0).roundToInt()))
            list.add(ModelCommunication(Constants.LAST_TIME, "" + Utility.getPreferences(this, SPConstants.LAST_TIME, "").toString()))
            list.add(ModelCommunication(Constants.TOTAL_FARE, "" + (initialFare.toDouble()+calculatedFare.toDouble()).roundToInt()))
            list.add(ModelCommunication(Constants.LAST_LAT, lastTickLocation.latitude.toString()))
            list.add(ModelCommunication(Constants.LAST_LNG, lastTickLocation.longitude.toString()))
            list.add(ModelCommunication(Constants.RIDE_DISTANCE, distance))
            list.add(ModelCommunication(Constants.TOTAL_TIME, "" + binding.chronometer.text.toString()))
            NetworkCall.callAPI(this, getService().changeRideStatus(APIConstants.BEARER + Utility.getUser(this).token, getJson(list)), this, true, APIConstants.CHANGE_RIDE_STATUS)
        } catch (e: Exception) {
            showLog("End Ride issue" + e.localizedMessage)
//                        var fare=Utility.getPreferences(this, SPConstants.INITIAL_FARE, "").toString()
//                        if (fare.isEmpty()){
//                            fare="0.0"
//                        }
//                        list.add(ModelCommunication(Constants.USER_ID, Utility.getUser(this).id.toString()))
//                        list.add(ModelCommunication(Constants.INITIAL_FARE, "" + fare.toDouble().roundToInt()))
//                        list.add(ModelCommunication(Constants.STATUS, Constants.END_RIDE))
//                        list.add(ModelCommunication(Constants.TOTAL_FARE,"" + fare.toDouble().roundToInt()))
//                        NetworkCall.callAPI(this, getService().changeRideStatus(APIConstants.BEARER + Utility.getUser(this).token, getJson(list)), this, true, APIConstants.CHANGE_RIDE_STATUS)

        }
    }

    override fun onBackPressed() {
            Utility.getAlertDialoge(this, "Close Application?", "You really want to close application ?").setPositiveButton("Ok") { dialog, which ->
                finish()
            }.setNegativeButton("No") { dialog, which ->
                dialog.dismiss()
            }.show()


        }

}