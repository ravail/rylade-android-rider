package com.senarios.ryladerider.activities

import android.view.View
import androidx.databinding.DataBindingUtil
import com.google.gson.reflect.TypeToken
import com.senarios.rylade.costants.Messages
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.rylade.retrofit.ApiResponse
import com.senarios.ryladerider.R
import com.senarios.ryladerider.adaptors.AdaptorFAQ
import com.senarios.ryladerider.adaptors.RecyclerViewCallback
import com.senarios.ryladerider.databinding.ActivityFaqBinding
import com.senarios.ryladerider.model.FaqModel
import com.senarios.ryladerider.model.ModelBaseResponse
import com.senarios.ryladerider.retrofit.NetworkCall
import com.senarios.ryladerider.utility.Utility
import kotlinx.android.synthetic.main.toolbar.view.*
import retrofit2.Response
import java.lang.reflect.Type

class FAQActivity : BaseAcivity() ,ApiResponse, RecyclerViewCallback,View.OnClickListener{
    private lateinit var binding: ActivityFaqBinding
    private lateinit var adaptor: AdaptorFAQ


    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this, R.layout.activity_faq)
    }

    override fun init() {
        binding.toolbar.toolbar.title="FAQ's"
        binding.toolbar.toolbar.setNavigationOnClickListener(this)
        NetworkCall.callAPI(this,getService().getFAQ(),this,true, APIConstants.GET_FAQ)
    }

    override fun OnCancelPermissioDialog() {

     }

    override fun OnPermissionApproved() {

     }



    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {

        val listType: Type = object : TypeToken<List<FaqModel>>() {}.getType()
        val list= Utility.getgson().fromJson<MutableList<FaqModel>>(response, listType)
        adaptor= AdaptorFAQ(this@FAQActivity,list,this)
        binding.recyclerView.adapter=adaptor
    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialog(this@FAQActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        Utility.showErrorDialog(this@FAQActivity,R.layout.dialog_error_505, Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialog(this@FAQActivity,R.layout.dialog_error, Messages.ERROR_TITLE,getString(R.string.something_went_wrong))
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialog(this@FAQActivity)
    }

    override fun onClick(v: View?) {
        finish()
    }
}