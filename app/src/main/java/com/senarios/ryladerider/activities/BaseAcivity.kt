package com.senarios.ryladerider.activities

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import com.senarios.rylade.costants.Codes
import com.senarios.rylade.costants.Tags
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.rylade.retrofit.DataService
import com.senarios.ryladerider.R
import com.senarios.ryladerider.callbacks.ActivityFragment
import com.senarios.ryladerider.callbacks.ActivityStates
import com.senarios.ryladerider.dialogfragments.DialogeCallback
import com.senarios.ryladerider.retrofit.Retrofit
import com.senarios.ryladerider.utility.Utility
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


/*custom base class responsible for handling same code which will be used in every class, every activity in this project
* will be extended from this class*/
abstract class BaseAcivity : AppCompatActivity() , ActivityFragment, Tags,Utility ,DialogeCallback{
    private lateinit var activityStates: ActivityStates;



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        keepScreenOn()
        setBinding()
        init()
    }


    abstract fun setBinding()

    protected abstract fun init()

    fun keepScreenOn() {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

     fun getAlertDialoge(context: Context?, title: String?, message: String?): AlertDialog.Builder? {
        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle(title)
        alertDialog.setMessage(message)
        return alertDialog
    }




     fun getCallback():ActivityStates{
        return activityStates
    }

    override fun get(activityStates: ActivityStates) {
        this.activityStates=activityStates;
     }

    fun showToast(context: Context?,message:String){
        Toast.makeText(context,message, Toast.LENGTH_LONG).show()
    }

    fun getService(): DataService {
        return  Retrofit.getinstance(APIConstants.BASE_URL).getService()
    }

    fun showDialog(dialogFragment: DialogFragment){
        dialogFragment.show(supportFragmentManager,"")
    }
    open fun hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            inputMethodManager?.hideSoftInputFromWindow(getCurrentFocus()!!.getWindowToken(), 0
            )
        }
    }

    open fun showSoftKeyboard() {
        val imm =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.toggleSoftInput(
            InputMethodManager.SHOW_FORCED,
            0
        )
    }

//    override fun OnChange(fragment: BaseDialogeFragment) {
//        fragment.show(supportFragmentManager,"dialog")
//    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: kotlin.IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
       when (requestCode) {
           0 -> {
               handleAllPermissionResult()
           }
           1 -> {
               handleSmsPermissionResult()
           }

           2 -> {
               handleLocationPermissionResult()
           }

           3-> {
               handleStoragePermissionResult()
           }

           4-> {
               handleCameraPermissionResult()
           }
           5-> {
                 handleMediaPermissionResult()
           }
           Codes.CONTACTS_PERMISSION_REQUEST->{
               handleContactPermissionResult()
           }
       }

    }




    fun showPermissionDialog(title:String,message:String,permission:Array<String>,isSetting:Boolean,code:Int){
       val builder= Utility.getAlertDialoge(this,title,message)
        if (isSetting){
            builder.setPositiveButton("Go to Setting") { p0, p1 ->
                Utility.goToSettings(this)
            }
        }
        else {
            builder.setPositiveButton("Alright") { p0, p1 ->
                ActivityCompat.requestPermissions(
                    this,
                    permission,
                    code
                );
            }
        }
            builder  .setNegativeButton("Nah, I'm good"
            ) { p0, p1 -> p0?.dismiss()
            OnCancelPermissioDialog()
            }
        builder.show()
    }

    abstract fun OnCancelPermissioDialog()

    abstract fun OnPermissionApproved()

    open fun hasPermissions(context: Context?, permissions: Array<String>): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (permission in permissions) {
                if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    return false

                }

            }
        }
        return true
    }



    fun checkMYPermission() {
        if(!hasPermissions(this, Utility.getPermissions())){
            ActivityCompat.requestPermissions(this, Utility.getPermissions(),0)
        }
    }
    fun checkSMSPermission() {
        if(!hasPermissions(this, arrayOf(Manifest.permission.RECEIVE_SMS))){
            ActivityCompat.requestPermissions(this,arrayOf(Manifest.permission.RECEIVE_SMS) ,1)
        }
    }
    fun checkContactPermission() {
        if(!hasPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS))){
            ActivityCompat.requestPermissions(this,arrayOf(Manifest.permission.READ_CONTACTS) ,Codes.CONTACTS_PERMISSION_REQUEST)
        }
    }
    fun checkLocationPermission() {
        if(!hasPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION))){
            ActivityCompat.requestPermissions(this,arrayOf(Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION) ,2)
        }
    }
    fun checkStoragePermission() {
        if(!hasPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE))){
            ActivityCompat.requestPermissions(this,arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE) ,3)
        }
    }
    fun checkCameraPermission() {
        if(!hasPermissions(this, arrayOf(Manifest.permission.CAMERA))){
            ActivityCompat.requestPermissions(this,arrayOf(Manifest.permission.CAMERA) ,4)
        }
    }
    fun checkMediaPermission() {
        if(!hasPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA))){
            ActivityCompat.requestPermissions(this,arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE) ,5)
        }
    }



    fun handleAllPermissionResult(){
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.RECEIVE_SMS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.RECEIVE_SMS)){
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.sms_permission),
                    arrayOf(Manifest.permission.RECEIVE_SMS),
                    false,0
                )
            }
            else{
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.sms_permission),
                    arrayOf(Manifest.permission.RECEIVE_SMS),
                    true,0
                )
            }

        } else if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.ACCESS_FINE_LOCATION)) {
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.location_permission),
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    false,0
                )
            }
            else{
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.location_permission),
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    true,0
                )
            }
        } else if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.media_permission),
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    false,0
                )
            }
            else{
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.media_permission),
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    true,0
                )
            }
        } else if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CAMERA)) {
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.camera_permission),
                    arrayOf(Manifest.permission.CAMERA),
                    false,0
                )
            }
            else{
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.camera_permission),
                    arrayOf(Manifest.permission.CAMERA),
                    true,0
                )
            }
        }
    }
     fun handleLocationPermissionResult(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)  {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)|| ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            showPermissionDialog(getString(R.string.permission_title), getString(R.string.location_permission), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), false,2)

            }
            else{
                showPermissionDialog(getString(R.string.permission_title), getString(R.string.location_permission), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), true,2)
            }
            }
            else{
                OnPermissionApproved()
            }

    }
     fun handleMediaPermissionResult(){
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)||(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_EXTERNAL_STORAGE)|| ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CAMERA)) {
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.camera_storage_permission),
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ),
                    false,5
                )
            }
            else{
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.camera_storage_permission),
                    arrayOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ),
                    true,5
                )
            }
        }
        else{
            OnPermissionApproved()
        }

    }
     fun handleCameraPermissionResult(){
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.CAMERA)) {
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.camera_permission),
                    arrayOf(Manifest.permission.CAMERA),
                    false,4
                )
            }
            else{
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.camera_permission),
                    arrayOf(Manifest.permission.CAMERA),
                    true,4
                )
            }
        }
        else{
            OnPermissionApproved()
        }
    }
     fun handleStoragePermissionResult(){
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.READ_EXTERNAL_STORAGE)) {
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.media_permission),
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    false,3
                )
            }
            else{
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.media_permission),
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    true,3
                )
            }
        }
        else{
            OnPermissionApproved()
        }
    }
    fun handleSmsPermissionResult(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.RECEIVE_SMS
                )
            ) {
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.sms_permission),
                    arrayOf(Manifest.permission.RECEIVE_SMS),
                    false, 1
                )
            } else {
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.sms_permission),
                    arrayOf(Manifest.permission.RECEIVE_SMS),
                    true, 1
                )
            }
        }
        else{
            OnPermissionApproved()
        }
    }
    private fun handleContactPermissionResult() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    this,
                    Manifest.permission.READ_CONTACTS
                )
            ) {
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.contact_Permission),
                    arrayOf(Manifest.permission.READ_CONTACTS),
                    false, Codes.CONTACTS_PERMISSION_REQUEST
                )
            } else {
                showPermissionDialog(
                    getString(R.string.permission_title),
                    getString(R.string.contact_Permission),
                    arrayOf(Manifest.permission.READ_CONTACTS),
                    true, Codes.CONTACTS_PERMISSION_REQUEST
                )
            }
        }
        else{
            OnPermissionApproved()
        }
    }

    fun getMissingFieldDialog(){
        Utility.showErrorDialog(this,R.layout.dialog_error_fields,getString(
            R.string.missing_field_title),getString(R.string.missing_field_message))
    }

     fun getPart(data:String,name:String): MultipartBody.Part{
        val file = File(data)
        val filepart: RequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        return MultipartBody.Part.createFormData(name, file.name, filepart)
    }




}
