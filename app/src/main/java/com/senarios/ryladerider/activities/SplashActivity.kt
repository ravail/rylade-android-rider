package com.senarios.ryladerider.activities

import android.animation.Animator
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.databinding.DataBindingUtil
import com.senarios.ryladerider.R
import com.senarios.ryladerider.databinding.ActivitySplashBinding

class SplashActivity : BaseAcivity(){
    private lateinit var binding: ActivitySplashBinding



    override fun setBinding() {
        binding=DataBindingUtil.setContentView(this, R.layout.activity_splash)
    }

    override fun init() {
        binding.splashImage.animate().alpha(1f)
            .setListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {

                }

                override fun onAnimationEnd(animation: Animator?) {
                  binding.splashImage.visibility= View.VISIBLE
                    Handler().postDelayed(Runnable {
                        finish()
                        startActivity(Intent(this@SplashActivity,LoginActivity::class.java))

                    },2000)
                }

                override fun onAnimationCancel(animation: Animator?) {

                }

                override fun onAnimationStart(animation: Animator?) {

                }

            })
            .duration=1000
    }

    override fun OnCancelPermissioDialog() {

    }

    override fun OnPermissionApproved() {

    }

    override fun OnTrigger() {

    }
}
