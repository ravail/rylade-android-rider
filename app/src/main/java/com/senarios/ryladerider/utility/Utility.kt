

package com.senarios.ryladerider.utility

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Build
import android.os.Environment
import android.provider.Settings
import android.util.Log
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.tntkhang.fullscreenimageview.library.FullScreenImageViewActivity
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.AutocompleteSessionToken
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.senarios.rylade.costants.Messages
import com.senarios.rylade.costants.SPConstants
import com.senarios.rylade.retrofit.DataService
import com.senarios.ryladerider.R
import com.senarios.ryladerider.model.ModelCommunication
import com.senarios.ryladerider.model.User
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.ryladerider.retrofit.Retrofit
import io.karn.notify.Notify
import net.alhazmy13.mediapicker.Image.ImagePicker
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.greenrobot.eventbus.EventBus
import java.io.*
import java.util.*


/*custom util class which has static functions for helping in certain tasks */
interface Utility {
    companion object {
        private const val TAG="UTL"
        private lateinit var sessionInstance: AutocompleteSessionToken

        fun showToast(context: Context?, message: String) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }

        fun getAlertDialoge(
            context: Context?,
            title: String?,
            message: String?
        ): MaterialAlertDialogBuilder {
            val alertDialog = MaterialAlertDialogBuilder(
                context!!,
                R.style.Theme_MaterialComponents_Light_Dialog_Alert
            )
            alertDialog.setTitle(title)
            alertDialog.setMessage(message)
            alertDialog.setCancelable(false)
            return alertDialog
        }

        fun validateET(editText: EditText): Boolean {
            return editText.text.toString().trim().isEmpty()
        }

        fun isLastItemDisplaying(recyclerView: RecyclerView): Boolean
        {
            if (recyclerView.adapter!!.itemCount != 0)
            {
                val lastVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager?)!!.findLastCompletelyVisibleItemPosition()
                return lastVisibleItemPosition != RecyclerView.NO_POSITION && lastVisibleItemPosition == recyclerView.adapter!!.itemCount - 1
            }
            return false
        }

        fun setErrorET(editText: EditText) {
            editText.error = Messages.EMPTY_FIELD
            editText.requestFocus()
        }

        fun setRErrorET(editText: EditText, message: String) {
            editText.error = message
            editText.requestFocus()
        }

        fun getTextET(editText: EditText?): String {
            return editText?.text.toString().trim()
        }

        fun isNetworkConnected(context: Context): Boolean {
            var result = false
            val cm =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (cm != null) {
                    val capabilities =
                        cm.getNetworkCapabilities(cm.activeNetwork)
                    if (capabilities != null) {
                        if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                            result = true
                        } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                            result = true
                        }
                    }
                }
            } else {
                if (cm != null) {
                    val activeNetwork = cm.activeNetworkInfo
                    if (activeNetwork != null) { // connected to the internet
                        if (activeNetwork.type == ConnectivityManager.TYPE_WIFI) {
                            result = true
                        } else if (activeNetwork.type == ConnectivityManager.TYPE_MOBILE) {
                            result = true
                        }
                    }
                }
            }
            return result
        }

        fun setErrorOnET(editText: EditText, message: String) {
            editText.error = message
            editText.requestFocus()
        }

        fun getJson(list: List<ModelCommunication>): JsonObject {
            val json = JsonObject()
            for (value in list) {
                json.addProperty(value.key, value.value)
            }
            Log.v("json", json.toString())
            return json
        }

        fun getJsonWithInt(list: List<ModelCommunication>): JsonObject {
            val json = JsonObject()
            for (value in list) {
                json.addProperty(value.key, value.value)
            }
            Log.v("json", json.toString())
            return json
        }

        fun showErrorDialog(
            context: Context?,
            layour_ID: Int,
            title: String?,
            message: String
        ): androidx.appcompat.app.AlertDialog {
            val errordialoge = getAlertDialoge(context!!, title, message)
            errordialoge.setView(layour_ID)
            errordialoge.setPositiveButton(
                "Ok"
            ) { p0, p1 -> p0?.dismiss() }
            return errordialoge.show()

        }

        fun showErrorDialogBuilder(
            context: Context?,
            layour_ID: Int,
            title: String?,
            message: String
        ): androidx.appcompat.app.AlertDialog.Builder {
            val errordialoge = getAlertDialoge(context!!, title, message)
            errordialoge.setView(layour_ID)
            errordialoge.setPositiveButton(
                "Ok"
            ) { p0, p1 -> p0?.dismiss() }
            return errordialoge

        }

        fun showNErrorDialogwithFinish(context: Context?): androidx.appcompat.app.AlertDialog {
            val networkDialoge =
                getAlertDialoge(context!!, Messages.NETWORK_ERROR_TITLE, Messages.NETWORK_ERROR)
            networkDialoge.setView(R.layout.dialog_network_error)
            networkDialoge.setPositiveButton(
                "Ok"
            ) { p0, p1 ->
                p0?.dismiss()
                (context as Activity).finish()
            }
                .setNegativeButton(
                    "GO to settings"
                ) { p0, p1 -> context.startActivity(Intent(WifiManager.ACTION_PICK_WIFI_NETWORK)) }
            return networkDialoge.show()
        }

        fun showNErrorDialog(context: Context?): androidx.appcompat.app.AlertDialog {
            val networkDialoge =
                getAlertDialoge(context!!, Messages.NETWORK_ERROR_TITLE, Messages.NETWORK_ERROR)
            networkDialoge.setView(R.layout.dialog_network_error)
            networkDialoge.setPositiveButton(
                "Ok"
            ) { p0, p1 -> p0?.dismiss() }
                .setNegativeButton(
                    "GO to settings"
                ) { p0, p1 -> context.startActivity(Intent(WifiManager.ACTION_PICK_WIFI_NETWORK)) }
            return networkDialoge.show()
        }


        fun setPreferences(context: Context?, key: String?, `object`: Any?) {
            val preference = context?.getSharedPreferences(SPConstants.SP, SPConstants.MODE)
            if (preference != null) {
                if (`object` is String) {
                    preference.edit().putString(key, `object` as String?).apply()
                } else if (`object` is Int) {
                    preference.edit().putInt(key, `object`).apply()
                } else if (`object` is Boolean) {
                    preference.edit().putBoolean(key, `object`).apply()
                }
//                else if (`object` is Long){
//                    preference.edit().putLong(key, `object`).apply()
//                }
//                else if (`object` is Float){
//                    preference.edit().putFloat(key, `object`).apply()
//                }
            }

        }

        fun getPreferences(context: Context?, key: String?, `object`: Any?): Any? {
            val preference = context?.getSharedPreferences(SPConstants.SP, SPConstants.MODE)
            if (preference != null) {
                if (`object` is String) {
                    return preference.getString(key, "")
                }
                else if (`object` is Int) {
                    return preference.getInt(key, 0)
                }
                else if (`object` is Boolean) {
                    return preference.getBoolean(key, false)
                }
//                else if (`object` is Long) {
//                    return preference.getLong(key, 0)
//                }
//                else if (`object` is Float) {
//                    return preference.getFloat(key, 0)
//                }

            }
            return ""
        }

        fun getgson(): Gson {
            return Gson().newBuilder()
                .setLenient()
                .setPrettyPrinting()
                .serializeNulls()
                .create()
        }


        fun removeEvents() {
            val event = EventBus.getDefault().getStickyEvent(ModelCommunication::class.java)
            if (event != null) {
                EventBus.getDefault().removeStickyEvent(event)
            }
        }

        fun removeStringEvents() {
            val event = EventBus.getDefault().getStickyEvent(String::class.java)
            if (event != null) {
                EventBus.getDefault().removeStickyEvent(event)
            }
        }

        fun getMediaBuilder(activity: Activity): ImagePicker.Builder {
            return ImagePicker.Builder(activity)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.HARD)
                .scale(800, 800)
                .directory(ImagePicker.Directory.DEFAULT)
                .allowMultipleImages(false)
                .enableDebuggingMode(true)
        }

        fun getPermissions(): Array<String> {
            return arrayOf(
                Manifest.permission.RECEIVE_SMS,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        }


        fun hasPermissions(
            context: Context?, permissions: Array<String>
        ): Boolean {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
                for (permission in permissions) {
                    if (context.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                        return false

                    }

                }
            }
            return true
        }

        fun goToSettings(context: Context?) {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            val uri: Uri = Uri.fromParts("package", context!!.getPackageName(), null)
            intent.data = uri
            context.startActivity(intent)
        }

        fun getPreference(context: Context?): SharedPreferences {
            return context!!.getSharedPreferences(SPConstants.SP, SPConstants.MODE)
        }

        fun checkIfUserLogged(context: Context?): Boolean {
            val user = getgson().fromJson<User>(getPreferences(context, SPConstants.USER, "") as String, User::class.java)
            return user != null
        }

        fun getService(): DataService {
            return Retrofit.getinstance(APIConstants.BASE_URL).getService()
        }

        fun hasLocationPermission(context: Context?): Boolean {
            return hasPermissions(
                context, arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            )
        }

        fun hasMediaPermission(context: Context?): Boolean {
            return hasPermissions(
                context,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
            )
        }

        fun hasSmSPermission(context: Context?): Boolean {
            return hasPermissions(context, arrayOf(Manifest.permission.RECEIVE_SMS))
        }

        fun hasContactPermission(context: Context?): Boolean {
            return hasPermissions(context, arrayOf(Manifest.permission.READ_CONTACTS))
        }

        fun checkLocationServices(context: Context?): Boolean {
            val locationManager =
                context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        }

        fun getUser(context: Context?): User {
            return getgson().fromJson(
                getPreference(context).getString(SPConstants.USER, ""),
                User::class.java
            )
        }

        fun showNotification(context: Context?, tl: String, tex: String) {
            Notify.with(context!!).content {
                title = tl
                text = tex
            }.show()
        }

        fun getPlacesSession(): AutocompleteSessionToken {
            if (!Companion::sessionInstance.isInitialized) {
                sessionInstance = AutocompleteSessionToken.newInstance()
            }
            return sessionInstance
        }

        fun getPlacesClient(context: Context?): PlacesClient {
            return Places.createClient(context!!)
        }

        fun getLatLng(context: Context?, text: String): LatLng? {
            val coder = Geocoder(context)
            var latLng: LatLng? = null
            latLng = try {
                val addresses = coder.getFromLocationName(text, 5)
                LatLng(addresses[0].latitude, addresses[0].longitude)
            } catch (e: Exception) {
                return null
            }
            return latLng
        }

        fun getAddress(context: Context?, latLng: LatLng): String? {
            val coder = Geocoder(context)
            try {
                val addresses = coder.getFromLocation(latLng.latitude, latLng.longitude, 5)
                return addresses[0].getAddressLine(0)
            } catch (e: Exception) {
                return null
            }
        }
        fun getAddressObj(context: Context?, latLng: LatLng): Address? {
            val coder = Geocoder(context)
            try {
                val addresses = coder.getFromLocation(latLng.latitude, latLng.longitude, 5)
                return addresses[0]
            } catch (e: Exception) {
                return null
            }
        }

        fun showOrderNotification(context: Context?) {
            showNotification(
                context,
                context!!.getString(R.string.order_notification_title),
                context!!.getString(R.string.order_notification_message)
            )
        }

        fun getGoogleService(): DataService {
            return Retrofit.getinstance(APIConstants.BASE_GOOGLE).getService()
        }


        fun checkType(id: Int): Boolean {
            when (id) {
                1 -> {
                    return true
                }
                2 -> {
                    return true
                }
                3 -> {
                    return true
                }
                4 -> {
                    return true
                }
                else -> {
                    return false
                }

            }
            return false
        }

        fun loadFullScreenImageView(context: Context, uri: String?) {
            val arrayList = ArrayList<String>()
            arrayList.add(uri!!)
            val fullImageIntent = Intent(context, FullScreenImageViewActivity::class.java)
            fullImageIntent.putStringArrayListExtra(
                FullScreenImageViewActivity.URI_LIST_DATA,
                arrayList
            )
            fullImageIntent.putExtra(FullScreenImageViewActivity.IMAGE_FULL_SCREEN_CURRENT_POS, 0)
            context.startActivity(fullImageIntent)
        }

        fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
            var vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
            vectorDrawable!!.setBounds(
                0,
                0,
                vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight()
            );
            var bitmap = Bitmap.createBitmap(
                vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(),
                Bitmap.Config.ARGB_8888
            );
            var canvas = Canvas(bitmap);
            vectorDrawable.draw(canvas);
            return BitmapDescriptorFactory.fromBitmap(bitmap);
        }

        fun isRiderAvailable(context: Context?): Boolean {
            return getUser(context).rider_availability == 1
        }

        public fun getPart(data: String): MultipartBody.Part {
            val file = File(data)
            val filepart: RequestBody =
                RequestBody.create(MediaType.parse("multipart/form-data"), file)
            return MultipartBody.Part.createFormData("photo", file.name, filepart)
        }

        public fun getFile(bitmap: Bitmap): File? { //you can create a new file name "test.jpg" in sdcard folder.
            val f = File(
                Environment.getExternalStorageDirectory()
                    .toString() + File.separator + "test.jpg"
            )
            try {
                f.createNewFile()
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            //Convert bitmap to byte array
            val bos = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos)
            val bitmapdata = bos.toByteArray()
            //write the bytes in file
            var fos: FileOutputStream? = null
            fos = try {
                FileOutputStream(f)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
                return null
            }
            try {
                fos.write(bitmapdata)
                fos.flush()
                fos.close()
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            }
            return f
        }
        fun getFCM(context: Context?):String{
            return getPreferences(context,SPConstants.FCM,"") as String
        }

         fun clearRideValues(context: Context?){
            getPreference(context).edit().remove(SPConstants.LAST_FARE).apply()
            getPreference(context).edit().remove(SPConstants.LAST_LAT_LNG).apply()
            getPreference(context).edit().remove(SPConstants.REMAINING_M).apply()
            getPreference(context).edit().remove(SPConstants.TOTAL_M).apply()
            getPreference(context).edit().remove(SPConstants.TIMER).apply()
             getPreference(context).edit().remove(SPConstants.LAST_TIME).apply()
             getPreference(context).edit().remove(SPConstants.INITIAL_FARE).apply()
             getPreference(context).edit().remove(SPConstants.IS_RIDE).apply()
             getPreference(context).edit().remove(SPConstants.NETWORK_BOOT_FLAG).apply()
             getPreference(context).edit().remove(SPConstants.RIDE).apply()
             getPreference(context).edit().remove(SPConstants.ORDER).apply()
             getPreference(context).edit().remove(SPConstants.CHRONO_TIMER).apply()
             removeValueFromSP(context!!,SPConstants.TIMER)



        }

        fun showLog(text: String){
            Log.v(TAG,text)
        }

        fun removeValueFromSP(context: Context,key:String){
            getPreference(context).edit().remove(key).apply()
        }

        fun getLongFromSP(context: Context?,key:String):Long{
            return getPreference(context).getLong(key,0)
        }


    }
}