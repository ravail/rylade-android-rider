package com.senarios.ryladerider

import android.app.Application
import android.content.Intent
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.android.libraries.places.api.Places
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessaging
import com.senarios.rylade.costants.SPConstants
import com.senarios.ryladerider.service.BackgroundLocation
import com.senarios.ryladerider.utility.Utility
import com.squareup.picasso.Picasso


class Rylade : Application(),OnSuccessListener<InstanceIdResult> {

    override fun onCreate() {
        super.onCreate()
        Places.initialize(this,resources.getString(R.string.Maps_Places_Key))
        Picasso.with(this).isLoggingEnabled=true
        FirebaseApp.initializeApp(this)
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this)
        FirebaseMessaging.getInstance().subscribeToTopic("test")
       // RxJavaPlugins.setErrorHandler { throwable: Throwable? -> } // nothing or some logging

    }

    override fun onSuccess(p0: InstanceIdResult?) {
        if (p0?.id!=null){
            Utility.setPreferences(this,SPConstants.FCM,p0.token)
        }
    }

}