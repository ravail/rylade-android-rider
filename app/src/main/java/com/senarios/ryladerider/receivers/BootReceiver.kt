package com.senarios.ryladerider.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import com.senarios.rylade.costants.SPConstants
import com.senarios.ryladerider.service.BackgroundLocation
import com.senarios.ryladerider.utility.Utility

class BootReceiver : BroadcastReceiver() {


    override fun onReceive(context: Context?, intent: Intent?) {


        if (intent!=null && intent.action!=null) {
            if(intent.action!!.equals("android.intent.action.BOOT_COMPLETED",true)) {
                if (Utility.hasLocationPermission(context)) {
                    if (Utility.checkIfUserLogged(context)) {
                        if (Utility.getPreferences(context, SPConstants.IS_RIDE, true) as Boolean) {
                            Utility.showLog("Broadcast Received")
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                context?.startForegroundService(Intent(context, BackgroundLocation::class.java))
                            } else {
                                context?.startService(Intent(context, BackgroundLocation::class.java)
                                )
                            }
                            Utility.showLog("Boot Receiver Called...")
                            if (context == null) {
                                Utility.showLog("Boot Receiver Context Null")
                            }

                        }
                    }
                }
            }
            else if (intent.action.equals("android.intent.action.QUICKBOOT_POWEROFF",true)
                ||
                intent.action.equals("android.intent.action.ACTION_SHUTDOWN",true)
            ){
                if (Utility.getPreference(context).contains(SPConstants.IS_RIDE)){
                    Utility.setPreferences(context,SPConstants.NETWORK_BOOT_FLAG,true)
                }
            }
        }

    }
}