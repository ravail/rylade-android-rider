package com.senarios.ryladerider

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import com.google.android.gms.maps.GoogleMapOptions
import com.google.android.gms.maps.MapView


class CustomMapView : MapView {
    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle) {}
    constructor(context: Context?, options: GoogleMapOptions?) : super(context, options) {}

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        /**
         * Request all parents to relinquish the touch events
         */
        parent.parent.requestDisallowInterceptTouchEvent(true)
        parent.requestDisallowInterceptTouchEvent(true)
        return super.dispatchTouchEvent(ev)
    }

//    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
//        when (event.action) {
//            MotionEvent.ACTION_DOWN -> if (null == parent) {
//                parent.parent.requestDisallowInterceptTouchEvent(true)
//                parent.requestDisallowInterceptTouchEvent(true)
//            } else {
//                parent.parent.requestDisallowInterceptTouchEvent(true)
//                parent.requestDisallowInterceptTouchEvent(true)
//            }
//            MotionEvent.ACTION_UP -> if (null == parent) {
//                parent.parent.requestDisallowInterceptTouchEvent(false)
//                parent.requestDisallowInterceptTouchEvent(true)
//            } else {
//                parent.parent.requestDisallowInterceptTouchEvent(false)
//                parent.requestDisallowInterceptTouchEvent(true)
//            }
//            else -> {
//            }
//        }
//        return super.onInterceptTouchEvent(event)
//    }
}