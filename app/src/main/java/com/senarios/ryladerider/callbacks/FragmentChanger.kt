package com.senarios.ryladerider.callbacks

import androidx.fragment.app.Fragment
/*this interface helps in changing fragments inside activity*/
interface FragmentChanger {
    fun OnChange(fragment: Fragment,tag:String)
}