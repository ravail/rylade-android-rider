package com.senarios.ryladerider.dialogfragments

import androidx.fragment.app.Fragment
import com.senarios.rylad.BaseDialogeFragment
/*interface which helps with communication between dialoge and acitvity*/
interface DialogeCallback {
    fun OnChange(fragment: BaseDialogeFragment){

    }
    fun OnTrigger(){

    }
}