package com.senarios.rylad

import android.Manifest


import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.senarios.rylade.costants.Messages
import com.senarios.rylade.retrofit.DataService
import com.senarios.ryladerider.R
import com.senarios.ryladerider.callbacks.ActivityFragment
import com.senarios.ryladerider.callbacks.ActivityStates
import com.senarios.ryladerider.dialogfragments.DialogeCallback
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.ryladerider.retrofit.Retrofit
import com.senarios.ryladerider.utility.Utility
import com.senarios.ryladerider.viewmodel.SharedVM


/**
 * A simple [Fragment] subclass.
 */

/*base class for dialog fragment which provide extra common functions*/
abstract class BaseDialogeFragment : Messages,BottomSheetDialogFragment(), ActivityStates {
    private lateinit var activityFragment : ActivityFragment
    private lateinit var sharedVM: SharedVM
    private lateinit var dialgoCallback: DialogeCallback
    private lateinit var behaviour: BottomSheetBehavior<*>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialgoCallback=context as DialogeCallback
        activityFragment=context as ActivityFragment
        activityFragment.get(this)
        sharedVM= ViewModelProvider(this).get(SharedVM::class.java)

    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val bottomSheetDialog =
            super.onCreateDialog(savedInstanceState) as BottomSheetDialog
       /* bottomSheetDialog.getWindow()?.setSoftInputMode(
            WindowManager.
            LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);*/

        bottomSheetDialog.setOnShowListener {
            val bottomSheet =
                bottomSheetDialog.findViewById<FrameLayout>(R.id.design_bottom_sheet)
            behaviour =BottomSheetBehavior.from<FrameLayout?>(bottomSheet!!)
            behaviour.state = BottomSheetBehavior.STATE_EXPANDED
            behaviour.isHideable=false
            init()
            behaviour.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) {

                }

                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                       /* behaviour.state = BottomSheetBehavior.STATE_EXPANDED*/
                    }
                }

            })

        }
        return bottomSheetDialog
    }

    override fun onResume() {
        super.onResume()

    }

    protected abstract fun init()

    fun expand() {
        if (::behaviour.isInitialized) {
            behaviour.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    fun collapse(){
        if (::behaviour.isInitialized) {
            behaviour.state = BottomSheetBehavior.STATE_COLLAPSED
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        return getFragmentView(inflater, container, savedInstanceState)
    }

    abstract fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?


    fun showToast(context: Context?,message:String){
        Toast.makeText(context,message, Toast.LENGTH_LONG).show()
    }


    fun getDialogCB():DialogeCallback{
        return dialgoCallback
    }

    fun getViewModel():SharedVM{
        return sharedVM
    }

    fun getService(): DataService {
        return  Retrofit.getinstance(APIConstants.BASE_URL).getService()
    }


    fun getFragmentChanger():DialogeCallback{
        return dialgoCallback
    }

    @Deprecated("")
    open fun isConnected(context: Context): Boolean {
        val manager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val is3g =
            manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting
        val isWifi =
            manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting
        return is3g && isWifi
    }

     fun hideSoftKeyboard() {

         val imm =
             context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
         imm?.toggleSoftInput(
             InputMethodManager.SHOW_FORCED,
             0
         )
    }

    fun showSoftKeyboard() {
        if (activity?.currentFocus != null) {
            val inputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            inputMethodManager?.hideSoftInputFromWindow(activity?.currentFocus!!.windowToken, 0
            )
        }}

     fun showHideSoftKeyboard() {
         if (activity?.currentFocus != null) {
             val inputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
             inputMethodManager?.hideSoftInputFromWindow(activity?.currentFocus!!.windowToken, 0
             )
         }
         else{
             val imm =
                 context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
             imm?.toggleSoftInput(
                 InputMethodManager.SHOW_FORCED,
                 0
             )
         }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.BottomSheetDialogTheme)

    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        showHideSoftKeyboard()

    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        showHideSoftKeyboard()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            0 -> {
                if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.RECEIVE_SMS
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.RECEIVE_SMS)){
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.sms_permission),
                            arrayOf(Manifest.permission.RECEIVE_SMS),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.sms_permission),
                            arrayOf(Manifest.permission.RECEIVE_SMS),
                            true
                        )
                    }

                } else if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.ACCESS_FINE_LOCATION)) {
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.location_permission),
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.location_permission),
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            true
                        )
                    }
                } else if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.media_permission),
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.media_permission),
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            true
                        )
                    }
                } else if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.CAMERA)) {
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.camera_permission),
                            arrayOf(Manifest.permission.CAMERA),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.camera_permission),
                            arrayOf(Manifest.permission.CAMERA),
                            true
                        )
                    }
                }
            }
            1 -> {
                if (!Utility.hasSmSPermission(this.context)){
                if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.RECEIVE_SMS)){
                    showPermissionDialog(
                        getString(R.string.permission_title),
                        getString(R.string.sms_permission),
                        arrayOf(Manifest.permission.RECEIVE_SMS),
                        false
                    )
                }
                else{
                    showPermissionDialog(
                        getString(R.string.permission_title),
                        getString(R.string.sms_permission),
                        arrayOf(Manifest.permission.RECEIVE_SMS),
                        true
                    )
                }
                }
            }

            2 -> {
                if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                )  if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.ACCESS_FINE_LOCATION)) {
                    showPermissionDialog(
                        getString(R.string.permission_title),
                        getString(R.string.location_permission),
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        false
                    )
                }
                else{
                    showPermissionDialog(
                        getString(R.string.permission_title),
                        getString(R.string.location_permission),
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        true
                    )
                }
            }

            3-> {
                if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.READ_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.media_permission),
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.media_permission),
                            arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                            true
                        )
                    }
                }
            }

            4-> {
                if (ContextCompat.checkSelfPermission(
                        this.context!!,
                        Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.CAMERA)) {
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.camera_permission),
                            arrayOf(Manifest.permission.CAMERA),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.camera_permission),
                            arrayOf(Manifest.permission.CAMERA),
                            true
                        )
                    }
                }
            }
            5-> {
                if (ContextCompat.checkSelfPermission(this.context!!, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.READ_EXTERNAL_STORAGE)|| ActivityCompat.shouldShowRequestPermissionRationale(this.activity!!,Manifest.permission.CAMERA)) {
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.camera_permission),
                            arrayOf(
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                            ),
                            false
                        )
                    }
                    else{
                        showPermissionDialog(
                            getString(R.string.permission_title),
                            getString(R.string.camera_permission),
                            arrayOf(
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                            ),
                            true
                        )
                    }
                }
            }
        }

    }


    fun showPermissionDialog(title:String,message:String,permission:Array<String>,isSetting:Boolean){
        val builder= Utility.getAlertDialoge(this.context!!,title,message)
        if (isSetting){
            builder.setPositiveButton("Go to Setting") { p0, p1 ->
                Utility.goToSettings(this.context!!)
            }
        }
        else {
            builder.setPositiveButton("Alright") { p0, p1 ->
                ActivityCompat.requestPermissions(
                    this.activity!!,
                    permission,
                    1
                );
            }
        }
        builder  .setNegativeButton("Nah, I'm good"
        ) { p0, p1 -> p0?.dismiss() }
        builder  .show()
    }

    open fun hasPermissions(context: Context?, permissions: Array<String>
    ): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (permission in permissions) {
                if (context.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    return false

                }

            }
        }
        return true
    }

    fun checkMYPermission() {
        if(!hasPermissions(this.context!!, Utility.getPermissions())){
            ActivityCompat.requestPermissions(this.activity!!, Utility.getPermissions(),0)
        }
    }
    fun checkSMSPermission() {
        if(!hasPermissions(this.context!!, arrayOf(Manifest.permission.RECEIVE_SMS))){
            ActivityCompat.requestPermissions(this.activity!!,arrayOf(Manifest.permission.RECEIVE_SMS) ,1)
        }
    }
    fun checkLocationPermission() {
        if(!hasPermissions(this.context!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION))){
            ActivityCompat.requestPermissions(this.activity!!,arrayOf(Manifest.permission.ACCESS_FINE_LOCATION) ,2)
        }
    }
    fun checkStoragePermission() {
        if(!hasPermissions(this.context!!, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE))){
            ActivityCompat.requestPermissions(this.activity!!,arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE) ,3)
        }
    }
    fun checkCameraPermission() {
        if(!hasPermissions(this.context!!, arrayOf(Manifest.permission.CAMERA))){
            ActivityCompat.requestPermissions(this.activity!!,arrayOf(Manifest.permission.CAMERA) ,4)
        }
    }

    fun checkMediaPermission() {
        if(!hasPermissions(this.context!!, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA))){
            ActivityCompat.requestPermissions(this.activity!!,arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA) ,5)
        }
    }
}
