package com.senarios.ryladerider.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.rylade.costants.Constants
import com.senarios.rylade.costants.Messages
import com.senarios.rylade.retrofit.ApiResponse
import com.senarios.ryladerider.R
import com.senarios.ryladerider.activities.OrderDetailsActivity
import com.senarios.ryladerider.adaptors.AdaptorEarning
import com.senarios.ryladerider.adaptors.RecyclerViewCallback
import com.senarios.ryladerider.databinding.FragmentEarningBinding
import com.senarios.ryladerider.model.*
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.ryladerider.retrofit.NetworkCall
import com.senarios.ryladerider.utility.Utility
import retrofit2.Response
import java.lang.Exception

class EarningFragment : BaseFragment() ,ApiResponse,RecyclerViewCallback{
    private lateinit var binding: FragmentEarningBinding
    private lateinit var adaptor:AdaptorEarning
    private lateinit var data:BaseEarningModel
    private lateinit var earningModel: EarningModel

    override fun init() {
         callEarningAPI()

        binding.recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener()
        {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (Utility.isLastItemDisplaying(binding.recyclerView)){
                    callPaginateEarningAPI()
                }

            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {

            }
        })
    }

    private fun callEarningAPI()
    {
        val list= mutableListOf<ModelCommunication>()
        list.add(ModelCommunication(Constants.COUNT,"0"))
        list.add(ModelCommunication(Constants.RIDER_ID,Utility.getUser(getReference()).id.toString()))
        NetworkCall.callAPI(getReference(),getBaseService().getRiderEarning(APIConstants.BEARER+Utility.getUser(getReference()).token,
            Utility.getJson(list)
            ),this,true,APIConstants.GET_RIDER_EARNING)
    }

    private fun callPaginateEarningAPI()
    {
        val list= mutableListOf<ModelCommunication>()
        list.add(ModelCommunication(Constants.COUNT,"1"))
        list.add(ModelCommunication(Constants.RIDER_ID,Utility.getUser(getReference()).id.toString()))
        list.add(ModelCommunication(Constants.DATEFROM,data.datefrom!!))
        list.add(ModelCommunication(Constants.DATETO,data.dateto!!))
        NetworkCall.callAPI(getReference(),getBaseService().getRiderEarning(APIConstants.BEARER+Utility.getUser(getReference()).token,
            Utility.getJson(list)
        ),this,true,APIConstants.GET_RIDER_EARNING)
    }

    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_earning,container,false)
        binding= DataBindingUtil.bind(view)!!
        init()
        return view
     }

    override fun OnBackPressed() {

    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try
        {
            data = Utility.getgson().fromJson<BaseEarningModel>(response, BaseEarningModel::class.java)
            if (data.data != null && data.data!!.isNotEmpty())
            {
                if (!::adaptor.isInitialized)
                {
                    earningModel = data.data!!.get(0)
                    binding.data = earningModel
                    val list=data.data!!.toMutableList()
                    list.removeAt(0)
                    adaptor = AdaptorEarning(getReference(),list, this)
                    binding.recyclerView.adapter = adaptor
                } else
                {
                    adaptor.addData(data.data!!)
                }

            }
        }
        catch (e:Exception){
            OnException(endpoint,e)
        }

    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialog(getReference(),R.layout.dialog_error_505,
            Messages.ERROR_TITLE,message)
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        Utility.showErrorDialog(getReference(),R.layout.dialog_error_505,
            Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialog(getReference(),R.layout.dialog_error, Messages.ERROR_TITLE,exception.localizedMessage!!)
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialog(getReference())
    }


}