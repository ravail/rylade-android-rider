package com.senarios.ryladerider.fragments

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.senarios.rylade.costants.Messages
import com.senarios.rylade.costants.SPConstants
import com.senarios.rylade.costants.Tags
import com.senarios.rylade.retrofit.ApiResponse
import com.senarios.ryladerider.R
import com.senarios.ryladerider.databinding.FragmentProfileBinding
import com.senarios.ryladerider.dialogfragments.EditPasswordDialog
import com.senarios.ryladerider.model.ModelBaseResponse
import com.senarios.ryladerider.model.ModelCommunication
import com.senarios.ryladerider.model.User
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.ryladerider.retrofit.NetworkCall
import com.senarios.ryladerider.utility.Utility
import retrofit2.Response

class ProfileFragment : BaseFragment(), TextView.OnEditorActionListener, ApiResponse,View.OnClickListener{
    private lateinit var binding: FragmentProfileBinding
    private lateinit var dialog: AlertDialog




    override fun init() {
        binding.data=Utility.getUser(getReference())
        binding.lastNameEdt.setOnEditorActionListener(this)
        binding.changePasswordEdt.setOnClickListener(this)
        binding.saveButton.setOnClickListener(this)
        binding.toolbar.setNavigationOnClickListener(this)
    }

    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_profile,container,false)
        binding= DataBindingUtil.bind(view)!!
        init()
        return view
     }

    override fun OnBackPressed() {
        getFragmentChanger().OnChange(CurrentOrdersFragment(),Tags.C_ORDER)
     }

    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        if (actionId== EditorInfo.IME_ACTION_DONE){
            validateInput()
            return true
        }
        return true
    }

    private fun validateInput() {
        val fname=Utility.getTextET(binding.firstNameEdt)
        val lname=Utility.getTextET(binding.lastNameEdt)
        if (fname.isEmpty()){
            Utility.setErrorET(binding.firstNameEdt)
        }
        else if (lname.isEmpty()){
            Utility.setErrorET(binding.lastNameEdt)
        }
        else if (!fname.equals(binding.data!!.f_name,true)||!lname.equals(binding.data!!.l_name,true)){
            val list= mutableListOf<ModelCommunication>()
            list.add(ModelCommunication("f_name",fname))
            list.add(ModelCommunication("l_name",lname))
            list.add(ModelCommunication("user_id",binding.data!!.id.toString()))
            NetworkCall.callAPI(getReference(),getBaseService().editprofile(APIConstants.BEARER+binding.data!!.token, Utility.getJson(list)),this,true,
                APIConstants.EDITPROFILE)
        }

    }

    override fun onClick(v: View?) {
        when(v?.id){
            binding.changePasswordEdt.id->{
                showDialog(EditPasswordDialog())
            }
            binding.saveButton.id->{
                validateInput()
            }
            else->{
                getFragmentChanger().OnChange(CurrentOrdersFragment(),Tags.C_ORDER)
            }
        }

    }
    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{
            val data=Utility.getgson().fromJson(response, User::class.java)
            Utility.setPreferences(getReference(), SPConstants.USER,response)
            showToast(getReference(),"Profile Updated Successfully")
            getFragmentChanger().OnChange(CurrentOrdersFragment(),Tags.C_ORDER)
        }
        catch (e:Exception){
            OnException(endpoint,e)
        }
    }

    override fun OnStatusfalse(endpoint:String, t: Response<ModelBaseResponse>, response: String, message: String) {
        dialog=  Utility.showErrorDialog(getReference(),R.layout.dialog_error_505,
            Messages.ERROR_TITLE,message)
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        dialog=  Utility.showErrorDialog(getReference(),R.layout.dialog_error_505,
            Messages.ERROR_TITLE,message)
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        dialog=  Utility.showErrorDialog(getReference(),R.layout.dialog_error, Messages.ERROR_TITLE,exception.localizedMessage!!)
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        dialog=    Utility.showNErrorDialog(getReference())
    }

    override fun onResume() {
        super.onResume()
        if (::dialog.isInitialized){
            dialog.dismiss()
        }
    }
}