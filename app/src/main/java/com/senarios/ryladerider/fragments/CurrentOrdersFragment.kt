package com.senarios.ryladerider.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.senarios.rylade.costants.Constants
import com.senarios.rylade.costants.Messages
import com.senarios.ryladerider.retrofit.APIConstants
import com.senarios.rylade.retrofit.ApiResponse
import com.senarios.ryladerider.R
import com.senarios.ryladerider.activities.OrderDetailsActivity
import com.senarios.ryladerider.adaptors.AdaptorOrders
import com.senarios.ryladerider.adaptors.RecyclerViewCallback
import com.senarios.ryladerider.databinding.FragmentOrdersBinding
import com.senarios.ryladerider.model.ModelBaseResponse
import com.senarios.ryladerider.model.ModelCommunication
import com.senarios.ryladerider.model.OrderListsModel
import com.senarios.ryladerider.model.OrderModel
import com.senarios.ryladerider.retrofit.NetworkCall
import com.senarios.ryladerider.utility.Utility
import com.senarios.ryladerider.utility.Utility.Companion.getService
import com.senarios.ryladerider.utility.Utility.Companion.getgson
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.Comparator

class CurrentOrdersFragment :BaseFragment(),ApiResponse,SwipeRefreshLayout.OnRefreshListener ,RecyclerViewCallback{
    private lateinit var binding:FragmentOrdersBinding
    private lateinit var adaptor: AdaptorOrders


    override fun onResume() {
        super.onResume()
        callOrderAPI()
    }

    override fun init() {

        binding.swipe.setOnRefreshListener(this)
    }


    override fun getFragmentView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_orders,container,false)
        binding=DataBindingUtil.bind(view)!!
        init()
        return view
    }

    override fun OnBackPressed() {
        if (activity!=null){
            activity!!.finish()
        }

    }


    private fun callOrderAPI() {
        val list:MutableList<ModelCommunication> = mutableListOf()
        list.add(ModelCommunication(Constants.RIDER_ID, Utility.getUser(this.context!!).id.toString()))
        NetworkCall.callAPI(this.context!!,getService().getOrders(APIConstants.BEARER+ Utility.getUser(this.context!!).token,Utility.getJson(list)),this, true, APIConstants.GET_CURRENT_ORDER)
    }

    override fun OnSuccess(endpoint: String, t: Response<ModelBaseResponse>, response: String) {
        try{
            val list:OrderListsModel= getgson().fromJson(response,OrderListsModel::class.java)
            Collections.sort(list.orders, object : Comparator<OrderModel> {
                override fun compare(o1: OrderModel?, o2: OrderModel?): Int {
                    val format= SimpleDateFormat("yyyy-MM-dd")
                    val date1=format.parse(o1!!.created_at!!.split(" ")[0])
                    val date2=format.parse(o2!!.created_at!!.split(" ")[0])
                    return date1.compareTo(date2)
                }
            })
            list.orders!!.reverse()
            adaptor=AdaptorOrders(this.context!!,list.orders,this,true)
            binding.currentOrdersRV.adapter=adaptor
            binding.swipe.isRefreshing=false
        }
        catch (e:Exception){
            OnException(endpoint,e)
        }

    }

    override fun OnStatusfalse(endpoint:String,t: Response<ModelBaseResponse>, response: String, message: String) {
        Utility.showErrorDialog(getReference(),R.layout.dialog_error_505,
            Messages.ERROR_TITLE,message)
        binding.swipe.isRefreshing=false
    }

    override fun OnError(endpoint:String,code: Int, message: String) {
        Utility.showErrorDialog(getReference(),R.layout.dialog_error_505,
            Messages.ERROR_TITLE,message)
        binding.swipe.isRefreshing=false
    }


    override fun OnException(endpoint:String,exception: Throwable) {
        Utility.showErrorDialog(getReference(),R.layout.dialog_error, Messages.ERROR_TITLE,exception.localizedMessage!!)
        binding.swipe.isRefreshing=false
    }

    override fun OnNetworkError(endpoint:String,message: String) {
        Utility.showNErrorDialog(getReference())
        binding.swipe.isRefreshing=false
    }

    override fun onRefresh() {
        callOrderAPI()
    }

    override fun OnClick(position: Int, any: Any) {
        startActivity(Intent(getReference(), OrderDetailsActivity::class.java).putExtra(Constants.ORDER_DATA,any as OrderModel))
    }

}