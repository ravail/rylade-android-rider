package com.senarios.rylade.retrofit

import com.google.gson.JsonObject
import com.senarios.rylade.costants.Constants
import com.senarios.ryladerider.model.ModelBaseResponse
import com.senarios.ryladerider.retrofit.APIConstants
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

/*interface which has abstract functions , these functions will be implemented by Retrofit */
interface DataService  {


    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.LOGIN)
    fun login( @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_CURRENT_ORDER)
    fun getOrders(@Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_PAST_ORDERS)
    fun getPOrders(@Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>


    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_ORDER_DETAIl)
    fun getOrderDetails(@Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.CHANGE_RIDER_AVAILABILITY)
    fun changeRiderStatus(@Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>


    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.CHANGE_ORDER_STATUS)
    fun changeOrderStatus(@Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>


    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.REJECT_ORDER)
    fun rejectOrder(@Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>


    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.ACCEPT_ORDER)
    fun acceptOrder(@Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Multipart
    @POST(APIConstants.COMPLETE_ORDER)
    fun completeOrder(@Header(APIConstants.AUTH)token:String,
                      @Part photo:MultipartBody.Part,
                      @Query(Constants.ORDER_ID) order_ID:String,
                      @Query(Constants.STATUS) status:String
    ):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_ADDRESS)
    fun getAddress(@Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>


    @POST(APIConstants.GET_CONTACT_US)
    fun getContactUS():Single<Response<ModelBaseResponse>>

    @POST(APIConstants.GET_FAQ)
    fun getFAQ():Single<Response<ModelBaseResponse>>

    @POST(APIConstants.GET_TAC)
    fun getTAC():Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.CHANGE_RIDE_STATUS)
    fun changeRideStatus(@Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.CHECK_RIDE_STATUS)
    fun checkRideStatus(@Header(APIConstants.AUTH)token:String, @Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.EDITPROFILE)
    fun editprofile( @Header(APIConstants.AUTH)token:String,@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.CHANGEPASSWORD)
    fun changepassword( @Header(APIConstants.AUTH)token:String,@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.GET_RIDER_EARNING)
    fun getRiderEarning( @Header(APIConstants.AUTH)token:String,@Body json:JsonObject):Single<Response<ModelBaseResponse>>

    @Headers(APIConstants.CONTENT_TYPE)
    @POST(APIConstants.UPDATE_LOCATION)
    fun updateLocation( @Header(APIConstants.AUTH)token:String,@Body json:JsonObject):Single<Response<ModelBaseResponse>>
}