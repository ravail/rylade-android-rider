package com.senarios.ryladerider.retrofit

interface APIConstants {
    companion object{
/*http://rylade.senarios.co*/
        const val BASE_GOOGLE="https://maps.googleapis.com/maps/api/"
        const val BASE_URL="https://rylade.com/api/auth/"
        const val BASE_PARCEL_IMAGE="https://rylade.com/parcel_orders/"
        const val BASE_MEDICINE_IMAGE="https://rylade.com/medicine_prescriptions/"
        const val BASE_PRODUCT_PHOTO="https://rylade.com/product_images/"


        //rylade_rider endpoints
        const val LOGIN="Riderlogin"
        const val EDITPROFILE="updateProfile"
        const val CHANGEPASSWORD="updatePassword"
        const val GET_CURRENT_ORDER="get_rider_orders"
        const val GET_PAST_ORDERS="get_rider_past_orders"
        const val CHANGE_RIDER_AVAILABILITY="change_rider_availability"
        const val GET_ORDER_DETAIl="get_rider_order_details"
        const val CHANGE_ORDER_STATUS="change_order_status"
        const val COMPLETE_ORDER="change_order_complete"
        const val ACCEPT_ORDER="rider_accept_order"
        const val REJECT_ORDER="rider_cancel_order"
        const val GET_ADDRESS="get_address"
        const val GET_CONTACT_US="get_contacts"
        const val GET_FAQ="get_faqs"
        const val GET_TAC="get_tac"
        const val CHANGE_RIDE_STATUS="change_ride_status"
        const val CHECK_RIDE_STATUS="check_ride_status"
        const val GET_RIDER_EARNING="get_rider_monthly_salary"
            const val UPDATE_LOCATION="update_rider_location"




        //google endpoints
        const val DIRECTION="distancematrix/json"

        //config
        const val CONTENT_TYPE="Content-Type: application/json"
        const val AUTH="Authorization"
        const val BEARER="Bearer "
    }

}