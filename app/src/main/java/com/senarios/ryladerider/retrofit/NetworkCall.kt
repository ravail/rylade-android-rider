package com.senarios.ryladerider.retrofit

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.senarios.rylade.RyladeDialoge
import com.senarios.rylade.costants.Messages
import com.senarios.rylade.costants.Messages.Companion.NULL_BODY_ERROR
import com.senarios.rylade.retrofit.ApiResponse
import com.senarios.ryladerider.model.ModelBaseResponse
import com.senarios.ryladerider.utility.Utility
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

/*base class for calling apis , this calls gives callback using api response interface based on fetched response.*/
class NetworkCall {

    companion object {
        private val TAG=javaClass::class.java.name
        fun callAPI(context: Context, single: Single<Response<ModelBaseResponse>>, api: ApiResponse, isDialog:Boolean, endpoint:String) {
            if (Utility.isNetworkConnected(context)) {
                val dialog = RyladeDialoge(context)
                if (isDialog) {
                    dialog.show()
                }
                single.observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(object : SingleObserver<Response<ModelBaseResponse>> {
                        override fun onSuccess(t: Response<ModelBaseResponse>) {

                            Log.v(TAG, "body is" + t.body().toString())
                            Log.v(TAG, "code : " + t.code())
                            if (t.isSuccessful) {
                                if (t.code() == 200) {
                                    if (t.body() != null) {
                                        Log.v("tag", Gson().toJson(t.body()))
                                        if (t.body()!!.status && t.body()!=null){
                                            Log.v("tag", t.body()!!.data.toString())
                                            api.OnSuccess(endpoint,t, Utility.getgson().toJson(t.body()!!.data))
                                        }
                                        else if (t.body()!!.data!=null){
                                         api.OnStatusfalse(endpoint,t,
                                             Utility.getgson().toJson(t.body()!!.data),t.body()!!.message)
                                        }
                                        else if(t.body()!!.message!=null){
                                            api.OnError(endpoint,t.code(),t.body()!!.message)
                                        }
                                        else{
                                            api.OnError(endpoint,t.code(),NULL_BODY_ERROR)
                                        }
                                    } else {
                                        api.OnError(endpoint,t.code(),t.errorBody().toString())
                                    }
                                } else {
                                    api.OnError(endpoint,t.code(),t.errorBody().toString())
                                }

                            } else {
                                api.OnError(endpoint,t.code(),t.errorBody().toString())
                            }

                            if (isDialog) {
                                dialog.dismiss()
                            }
                        }

                        override fun onSubscribe(d: Disposable) {

                        }

                        override fun onError(e: Throwable) {
                            Log.v(TAG, "exception : " + e.localizedMessage)
                            api.OnException(endpoint,e)
                            if (isDialog) {
                                dialog.dismiss()
                            }
                        }

                    })
            }
            else{
                api.OnNetworkError(endpoint, Messages.NETWORK_ERROR)
            }


        }
    }

}