package com.senarios.rylade.costants

/*tags related to fragments*/

interface Tags {
    companion object{
        const val C_ORDER="Current Orders"
        const val P_ORDER="Past Orders"
        const val HOME: String="Home Screen"
        const val EARNING="Earnings"
        const val PROFILE="Profile"
        const val LOGIN_SIGN_UP="Login Signup Dialog Fragment"
        const val LOGIN="Login Dialog Fragment"
        const val REGISTER="Register Dialog Fragment"
        const val VERIFY_OTP="Verify OTP Dialog Fragment"
    }
}