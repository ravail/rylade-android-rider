package com.senarios.rylade.costants

import android.content.Context

/*constant strings related to shared preference*/

interface SPConstants {
    companion object{

        const val SP="Rylade_Rider"
        const val MODE= Context.MODE_PRIVATE
        const val USER="user_data"
        const val OTP="otpsms"
        const val IS_RIDE="is_ride"
        const val FCM="fcm"

        //ride calculation values
        const val LAST_LAT_LNG="last lat lng"
        const val LAST_FARE="last_fare"
        const val REMAINING_M="remaining m"
        const val TOTAL_M="total m"
        const val TIMER="waiting_time"
        const val INITIAL_FARE="initial fare"
        const val LAST_TIME="last time"
        const val NETWORK_BOOT_FLAG="network_boot_flag"
        const val ORDER="ordermodel"
        const val RIDE="ridemodel"
        const val CHRONO_TIMER="chronometer timer"
        const val START_TIME_LONG="start time ride"
    }
}