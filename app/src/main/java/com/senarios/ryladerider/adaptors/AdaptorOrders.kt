package com.senarios.ryladerider.adaptors

import android.content.Context
import android.location.Address
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.model.LatLng
import com.senarios.ryladerider.R
import com.senarios.ryladerider.databinding.ItemOrdersBinding
import com.senarios.ryladerider.model.OrderModel
import net.alhazmy13.mediapicker.Utility

/*listview inside myaddress screen, this class will show all the address we fetch from server*/

class AdaptorOrders (val context: Context, val list:MutableList<OrderModel>, val callback: RecyclerViewCallback, val isActive:Boolean): RecyclerView.Adapter<AdaptorOrders.holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.item_orders,parent,false)
        val binding: ItemOrdersBinding =DataBindingUtil.bind(view)!!
        return holder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
     }

    override fun onBindViewHolder(holder: holder, position: Int) {
        val b=holder.binding
        b.data= list[position]

        if (isActive){
            if (list[position].address!=null){
              val addressObj:Address? = com.senarios.ryladerider.utility.Utility.getAddressObj(context, LatLng(list[position].address!!.lat.toDoubleOrNull()!!,list[position].address!!.lon.toDoubleOrNull()!!))
                if (addressObj!=null){
                    if (!addressObj.subLocality.isNullOrEmpty()) {
                        b.location.text = addressObj.subLocality
                    }
                    else{
                        b.location.visibility=View.GONE
                    }
                }
                else{
                    b.location.visibility=View.GONE
                }

            }
            else{
                b.location.visibility=View.GONE
            }
        }
        else{
            b.location.visibility=View.GONE
        }

        if (list[position].order_type==1){
            holder.binding.name.text = "Medicine"
        }
        else if (list[position].order_type==2){
            holder.binding.name.text = "Parcel Delivery"
        }
        else if (list[position].order_type==3){
            holder.binding.name.text = "Ride"
        }
        else if (list[position].order_type==4){
            holder.binding.name.text = "E-Commerce"
        }


    }



   inner class holder(val binding: ItemOrdersBinding) : RecyclerView.ViewHolder(binding.root) ,View.OnClickListener {
       init {
           binding.cardview.setOnClickListener(this)
       }

       override fun onClick(v: View?) {
           when (v?.id) {
               binding.cardview.id -> {
                   if (list.size>0) {
//                       if (isActive) {
                           callback.OnClick(adapterPosition, list[adapterPosition])
//                       }
                   }
               }
           }
       }
    }
}