package com.senarios.ryladerider.adaptors
/*interface responsible for handling callbacks on recyclerview item click inside fragment or activity its attached*/
interface RecyclerViewCallback {
    fun OnEdit(position:Int, any:Any){

    }

    fun OnDelete(position:Int,any:Any){

    }

    fun OnClick(position:Int,any:Any){

    }
    fun OnScroll(){

    }
    fun openGallery(position: Int){

    }
}