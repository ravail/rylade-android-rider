package com.senarios.ryladerider.adaptors

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.senarios.ryladerider.R
import com.senarios.ryladerider.databinding.ItemEarningBinding
import com.senarios.ryladerider.databinding.ItemFaqBinding
import com.senarios.ryladerider.model.EarningModel
import com.senarios.ryladerider.model.FaqModel

/*listview inside myaddress screen, this class will show all the address we fetch from server*/

class AdaptorEarning (val context: Context, val list:MutableList<EarningModel>, val callback: RecyclerViewCallback): RecyclerView.Adapter<AdaptorEarning.holder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): holder {
        val view=LayoutInflater.from(parent.context).inflate(R.layout.item_earning,parent,false)
        val binding: ItemEarningBinding =DataBindingUtil.bind(view)!!
        return holder(binding)
    }

    override fun getItemCount(): Int {
        return list.size
     }

    override fun onBindViewHolder(holder: holder, position: Int) {
        holder.binding.data=list[position]

    }

    fun addData(data: List<EarningModel>)
    {
        list.addAll(data)
        notifyDataSetChanged()
    }


    inner class holder(val binding: ItemEarningBinding) : RecyclerView.ViewHolder(binding.root)  {



    }
}